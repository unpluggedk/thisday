//
//  main.m
//  thisdaylite
//
//  Created by Jason Kim on 13. 4. 25..
//  Copyright (c) 2013년 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
