//
//  AppDelegate.h
//  thisdaylite
//
//  Created by Jason Kim on 13. 4. 25..
//  Copyright (c) 2013년 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
