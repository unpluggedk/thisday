//
//  ProgressViewWithLabel.m
//  ThisDate
//
//  Created by jason kim on 1/27/13.
//  Copyright (c) 2013 yogurtfire. All rights reserved.
//

#import "ProgressViewWithLabel.h"

@implementation ProgressViewWithLabel

@synthesize progressView;
@synthesize textLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = false;
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        textLabel = [[UILabel alloc] init];
        [textLabel setTextAlignment:UITextAlignmentCenter];
        [textLabel setBackgroundColor:[UIColor clearColor]];
        [textLabel setTextColor:[UIColor whiteColor]];
        [textLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [textLabel setShadowColor:[UIColor blackColor]];
        [self addSubview:progressView];
        [self addSubview:textLabel];
        self.frame = CGRectMake(0,0,progressView.bounds.size.width,progressView.bounds.size.height*3);
    }
    return self;
}


- (void)layoutSubviews{
    CGRect newFrame = self.bounds;
    newFrame.size.height -= progressView.frame.size.height;
    textLabel.frame = newFrame;
    newFrame = progressView.frame;
    newFrame.origin.y = self.bounds.size.height - newFrame.size.height;
    progressView.frame = newFrame;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
