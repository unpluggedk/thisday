//
//  ProgressViewWithLabel.h
//  ThisDate
//
//  Created by jason kim on 1/27/13.
//  Copyright (c) 2013 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressViewWithLabel : UIView{
    UIProgressView *progressView;
    UILabel *textLabel;
}

@property (nonatomic, retain) UIProgressView *progressView;
@property (nonatomic, retain) UILabel *textLabel;


@end
