//
//Copyright (c) 2011, Tim Cinel
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:
//* Redistributions of source code must retain the above copyright
//notice, this list of conditions and the following disclaimer.
//* Redistributions in binary form must reproduce the above copyright
//notice, this list of conditions and the following disclaimer in the
//documentation and/or other materials provided with the distribution.
//* Neither the name of the <organization> nor the
//names of its contributors may be used to endorse or promote products
//derived from this software without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//åLOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#import "ActionSheetDaysPicker.h"

@interface ActionSheetDaysPicker()
@property (nonatomic,strong) NSArray *data;
@property (nonatomic,assign) NSInteger selectedIndex_days;
@property (nonatomic,assign) NSInteger selectedIndex_beforeOrAfter;
@property (nonatomic,assign) NSInteger minDays;
@property (nonatomic,assign) NSInteger maxDays;

@end

@implementation ActionSheetDaysPicker
@synthesize data = _data;
@synthesize selectedIndex_days = _selectedIndex_days;
@synthesize selectedIndex_beforeOrAfter = _selectedIndex_beforeOrAfter;
@synthesize onActionSheetDone = _onActionSheetDone;
@synthesize onActionSheetCancel = _onActionSheetCancel;
@synthesize minDays = _minDays;
@synthesize maxDays = _maxDays;

- (void)selectedDays:(NSInteger)days beforeOrAfter:(NSInteger)b_or_a{
    _selectedIndex_days = days;
    _selectedIndex_beforeOrAfter = b_or_a;
}

+ (id)showPickerWithTitle:(NSString *)title from:(NSInteger)minDays till:(NSInteger)maxDays initialSelection:(NSInteger)index doneBlock:(ActionDaysDoneBlock)doneBlock cancelBlock:(ActionDaysCancelBlock)cancelBlockOrNil origin:(id)origin {
    ActionSheetDaysPicker * picker = [[ActionSheetDaysPicker alloc] initWithTitle:title from:minDays till:maxDays initialSelection:index doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title from:(NSInteger)minDays till:(NSInteger)maxDays initialSelection:(NSInteger)index doneBlock:(ActionDaysDoneBlock)doneBlock cancelBlock:(ActionDaysCancelBlock)cancelBlockOrNil origin:(id)origin {
    self = [self initWithTitle:title from:minDays till:maxDays intialSelection:index target:nil seccessAction:nil cancelAction:nil origin:origin];
    if (self) {
        self.onActionSheetDone = doneBlock;
        self.onActionSheetCancel = cancelBlockOrNil;
    }
    return self;
}

+ (id)showPickerWithTitle:(NSString *)title from:(NSInteger)minDays till:(NSInteger)maxDays initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin {
    ActionSheetDaysPicker *picker = [[ActionSheetDaysPicker alloc] initWithTitle:title from:minDays till:maxDays intialSelection:index target:target seccessAction:successAction cancelAction:cancelActionOrNil origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title from:(NSInteger)minDays till:(NSInteger)maxDays initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin {
    self = [self initWithTarget:target successAction:successAction cancelAction:cancelActionOrNil origin:origin];
    if (self) {
        //self.data = data;
        self.selectedIndex_days = index;
        self.selectedIndex_beforeOrAfter = 0;
        self.title = title;
    }
    return self;
}

- (id)initWithTitle:(NSString *)title from:(NSInteger)minDays till:(NSInteger)maxDays intialSelection:(NSInteger)index target:(id)target seccessAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin{
    self = [self initWithTarget:target successAction:successAction cancelAction:cancelActionOrNil origin:origin];
    if (self) {
//        self.data = data;
        self.minDays = minDays;
        self.maxDays = maxDays;
        self.selectedIndex_days = index;
        self.selectedIndex_beforeOrAfter = 0;
        self.title = title;
    }
    return self;
    
    
}
- (UIView *)configuredPickerView {
 //   if (!self.data)
 //       return nil;
    CGRect pickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
    UIPickerView *stringPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    stringPicker.delegate = self;
    stringPicker.dataSource = self;
    stringPicker.showsSelectionIndicator = YES;
    [stringPicker selectRow:self.selectedIndex_days inComponent:0 animated:NO];
    [stringPicker selectRow:self.selectedIndex_beforeOrAfter inComponent:1 animated:NO];
    
    //need to keep a reference to the picker so we can clear the DataSource / Delegate when dismissing
    self.pickerView = stringPicker;
    
    return stringPicker;
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)successAction origin:(id)origin {
    if (self.onActionSheetDone) {
        _onActionSheetDone(self, self.selectedIndex_days, self.selectedIndex_beforeOrAfter,
                           (self.selectedIndex_days == 0?@"Event day":[NSString stringWithFormat:@"%d", self.selectedIndex_days]),
                           (self.selectedIndex_beforeOrAfter == 0?@"before":@"after"));
        return;
    }
    else if (target && [target respondsToSelector:successAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:successAction
                     withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:self.selectedIndex_days], [NSNumber numberWithInt:self.selectedIndex_beforeOrAfter], nil]
                     withObject:origin];
#pragma clang diagnostic pop
        return;
    }
    NSLog(@"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(successAction));
}

- (void)notifyTarget:(id)target didCancelWithAction:(SEL)cancelAction origin:(id)origin {
    if (self.onActionSheetCancel) {
        _onActionSheetCancel(self);
        return;
    }
    else if (target && cancelAction && [target respondsToSelector:cancelAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:cancelAction withObject:origin];
#pragma clang diagnostic pop
    }
}

#pragma mark - UIPickerViewDelegate / DataSource

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(component == 0)
        self.selectedIndex_days = row;
    else
        self.selectedIndex_beforeOrAfter = row;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(component == 0)
        return self.maxDays - self.minDays;
    else
        return 2;
}

/*
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* label = (UILabel*)view;
    if (view == nil){
        UIFont *font = [UIFont boldSystemFontOfSize:23];
        
        label= [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, pickerView.frame.size
                                                         .height)];
        label.font = font;
        label.textAlignment = UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
    }
//    label.text = [self.data textForRow:row forComponent:component];
    label.text = [self.data objectAtIndex:row];
    return label;
}
*/

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(component == 0)
        return [NSString stringWithFormat:@"      %d Days", row];
    else
        return row==0?@"Before":@"After";
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if(component == 0)
        return 150;
    else
        return 100;
//    return pickerView.frame.size.width - 30;
}

#pragma mark - Block setters

// NOTE: Sometimes see crashes when relying on just the copy property. Using Block_copy ensures correct behavior

- (void)setOnActionSheetDone:(ActionDaysDoneBlock)onActionSheetDone {
    if (_onActionSheetDone) {
        _onActionSheetDone = nil;
    }
    _onActionSheetDone = onActionSheetDone;
}

- (void)setOnActionSheetCancel:(ActionDaysCancelBlock)onActionSheetCancel {
    if (_onActionSheetCancel) {
        _onActionSheetCancel = nil;
    }
    _onActionSheetCancel = onActionSheetCancel;
}

@end