//
//  SinglePageIndexData.h
//  ThisDate
//
//  Created by jason kim on 9/17/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SinglePageIndexData : NSObject{
    NSInteger index;
    NSInteger days;
}

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger days;

@end
