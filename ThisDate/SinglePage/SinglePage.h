//
//  SinglePage.h
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../DialController/MultiDialViewController.h"

@interface SinglePage : UIView <MultiDialViewControllerDelegate>{
    UIView *panel;
    UIImageView *bg;
    
    bool bIsDateSet;
    bool bIsTitleSet;

    NSString *title_;
    NSDate *date_;
    
    bool bAlarmEnabled;
    NSInteger alarmDays_;
    NSInteger alarmBeforeOrAfter_;
    NSDate *alarmTime_;
    
    NSInteger uid;
    
    NSInteger eventDaysLeft;
    bool bDdayPast;
    
//    MultiDialViewController *multiDialController;

    UIColor *panelSelectedColor;
    UIColor *panelUnselectedColor;

    
    //0: normal  1:dark
    int panelColor;
    
    bool bInView;
}

@property (nonatomic, retain) UIView *panel;
@property (nonatomic, strong) UIImageView *bg;
//@property (nonatomic, retain) MultiDialViewController *multiDialController;
@property (nonatomic, retain) UIColor *panelSelectedColor;
@property (nonatomic, retain) UIColor *panelUnselectedColor;
@property (nonatomic, assign) bool bIsDateSet;
@property (nonatomic, assign) bool bIsTitleSet;
@property (nonatomic, assign) bool bAlarmEnabled;
@property (nonatomic, assign) bool bInView;
@property (nonatomic, assign) int panelColor;

- (void)drawPanel;
- (void)refreshPanel;
- (void) setPanelColor:(int)color;

- (NSInteger) getEventDaysLeft;
- (NSInteger) getEventDaysLeftOrPast;
- (bool) getDdayPast;

//- (void)saveImageInBackground;
- (void)setBackgroundImage:(UIImage*)bgImage save:(bool)save;
- (void)updateQuality:(CGInterpolationQuality)quality;
- (UIImage*)getBgImage;

// title and date
- (void)setTitle:(NSString*)newTitle saveToDB:(bool)save;
- (void)setDate:(NSDate*)date saveToDB:(bool)save;
- (bool)isTitleSet;
- (bool)isDateSet;
- (NSString*)getTitle;
- (NSString*)getDateString;
- (NSString*)getDateStringShort;
- (NSDate*)getDate;

// alarm
- (NSInteger)getAlarmDays;
- (void)setAlarmDays:(NSInteger)value;
//- (NSInteger) getAlarmBeforeOrAfter;
//- (void)setAlarmBeforeOrAfter:(NSInteger)value;
- (NSDate*)getAlarmTime;
- (void)setAlarmTime:(NSDate*)value;

// image
- (NSInteger) getUID;
- (void) setUID:(NSInteger)newUid;

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;

@end
