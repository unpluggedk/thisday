//
//  SinglePage.m
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "SinglePage.h"
#import <QuartzCore/QuartzCore.h>
#import "AppData.h"
#import "../UIImage/UIImage+Resize.h"

#define BGIMAGE 101
#define DAYS 102
#define TITLE 103
#define LABEL 104
#define DATE 105

@implementation SinglePage

@synthesize panel;
@synthesize bg;
//@synthesize multiDialController;
@synthesize panelSelectedColor;
@synthesize panelUnselectedColor;
@synthesize bIsDateSet;
@synthesize bIsTitleSet;
@synthesize bAlarmEnabled;
@synthesize bInView;
@synthesize panelColor;

- (void)refreshPanel{
    NSArray *subviews_ = [panel subviews];
    for(int i=0; i<[subviews_ count]; i++){
        UIView *v = [subviews_ objectAtIndex:i];
        [v setNeedsDisplay];
    }
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
 
        panelColor = 0;
        
        // 400 x 160 
        bInView = false;
        panelSelectedColor = [UIColor grayColor];
        panelUnselectedColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1.0];
        
        // Initialization code
        bg = [[UIImageView alloc] init];
        [bg setTag:BGIMAGE];
        [bg setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        [self addSubview:bg];
        
        [self setClipsToBounds:YES];
        
        //panel = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 200, 80)];

        panel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BabyA00_Pannel01_1_01"]];
        [panel setFrame:CGRectMake(10,10,200,80)];
        [panel setUserInteractionEnabled:true];
        //[panel setBackgroundColor:panelUnselectedColor];
        [panel.layer setCornerRadius:5.0f];

        // labels
        
        UILabel *num = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 85, 30)];
        [num setTag:DAYS];
        // [num setFont:[UIFont systemFontOfSize:20.0f]];
        [num setFont:[UIFont fontWithName:@"GillSans-Bold" size:20.0f]];
        [num setTextColor:[UIColor whiteColor]];
        [num.layer setCornerRadius:3.0f];
        [num setTextAlignment:UITextAlignmentLeft];
        [num setText:@"12345"];
        [num setBackgroundColor:[UIColor clearColor]];
        [panel addSubview:num];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(90, 20, 100, 15)];
        [date setTag:DATE];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:15.0f]];
        [date setTextColor:[UIColor colorWithRed:1 green:215.0/255.0 blue:0 alpha:1.0]];
        [date setText:@"20120201"];
        //[text setMinimumFontSize:10.0f];
        [date setAdjustsFontSizeToFitWidth:YES];
        [date setTextAlignment:UITextAlignmentLeft];
        [panel addSubview:date];
        
        UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(12, 20, 100, 15)];
        [text setTag:LABEL];
        [text setBackgroundColor:[UIColor clearColor]];
        [text setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:15.0f]];
        [text setTextColor:[UIColor colorWithRed:1 green:215.0/255.0 blue:0 alpha:1.0]];
        [text setText:@"days left"];
        //[text setMinimumFontSize:10.0f];
        [text setAdjustsFontSizeToFitWidth:YES];
        [text setTextAlignment:UITextAlignmentLeft];
        [panel addSubview:text];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(90, 30, 100, 35)];
        [title setTag:TITLE];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTextColor:[UIColor whiteColor]];
       // [title setFont:[UIFont boldSystemFontOfSize:20.0f]];
       // [title setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:20.0f]];
        [title setFont:[UIFont fontWithName:@"NanumPen" size:30.0f]];
        [title setTextAlignment:UITextAlignmentLeft];
        [title setText:@"TITLE"];
        [title setAdjustsFontSizeToFitWidth:YES];
        [panel addSubview:title];
     /*
        multiDialController = [[MultiDialViewController alloc] init];
        [multiDialController.view setUserInteractionEnabled:NO];
        multiDialController.delegate = self;
        multiDialController.view.frame = CGRectMake(5, 0, 160, 50);
        multiDialController.view.clipsToBounds =true;
        
        [panel addSubview:multiDialController.view];
      */
        [panel setAlpha:0.7];
        [self addSubview:panel];
        
        bIsDateSet = false;
        bIsTitleSet = false;
        
        bAlarmEnabled = false;
        alarmDays_ = 0;
        alarmBeforeOrAfter_ = 0;
        alarmTime_ = [[AppData sharedAppData] dateWithZeroHMS:[NSDate date]];
        
        uid = -1;
    }
    return self;
}

- (void) setPanelColor:(int)color{
    [panel setAlpha:(color==0)?0.7:1.0];
    panelColor = color;
}

- (void)drawPanel{
    [(UILabel*)[panel viewWithTag:DATE] setText:[[AppData sharedAppData] dateToString:date_]];
    
    if(eventDaysLeft == 0){
        [(UILabel*)[panel viewWithTag:DAYS] setText:@"00000"];
        [(UILabel*)[panel viewWithTag:LABEL] setText:@"today"];
        return;
    }
    
    if(bDdayPast  && [[AppData sharedAppData] getCountFromOne]){
        NSString *eventDaysStr = [NSString stringWithFormat:@"%d", eventDaysLeft + 1];
        NSMutableString *result = [[NSMutableString alloc] init];
        
        int length = [eventDaysStr length];
        
        for(int i=length; i<5; i++)
            [result appendString:@"0"];
        
        for(int i=0; i<length-1; i++)
            [result appendFormat:@"%c", [eventDaysStr characterAtIndex:i]];
        
        [result appendFormat:@"%c", [eventDaysStr characterAtIndex:length-1]];
        UILabel *dayslabel = (UILabel*)[panel viewWithTag:DAYS];
        [dayslabel setText:result];
        
        UILabel *label = (UILabel*)[panel viewWithTag:LABEL];
        switch ([result characterAtIndex:[result length] - 1]) {
    /*        case '1':
                [label setText:@"st day since"];
                break;
            case '2':
                [label setText:@"nd day since"];
                break;
            case '3':
                [label setText:@"rd day since"];
                break;
     */
            default:
                [label setText:@"days past"];
                break;
        }
    }
    else{
        NSString *newDays = [NSString stringWithFormat:@"%d", eventDaysLeft];
        NSMutableString *result = [[NSMutableString alloc] init];;
        int length = [newDays length];
        for(int i=length; i<5; i++)
            [result appendString:@"0"];
        for(int i=0; i<length-1; i++)
            [result appendFormat:@"%c", [newDays characterAtIndex:i]];
        [result appendFormat:@"%c", [newDays characterAtIndex:length-1]];
        
        UILabel *label = (UILabel*)[panel viewWithTag:DAYS];
        [label setText:result];
        
        if(bDdayPast){
            UILabel *label = (UILabel*)[panel viewWithTag:LABEL];
            [label setText:@"days past"];
        }
        else{
            UILabel *label = (UILabel*)[panel viewWithTag:LABEL];
            [label setText:@"days left"];
        }
    }
}

- (void)updateQuality:(CGInterpolationQuality)quality{
    if(!bInView)
        return;
    UIImage *img = [UIImage imageWithData:[[AppData sharedAppData] readFromFile:[NSString stringWithFormat:@"%d.jpeg", uid]]];
    //[bg setContentMode:UIViewContentModeScaleAspectFill] ;

    //[bg setFrame:CGRectMake(-10.0, -10.0, 330, 580)];
    
    if(quality == kCGInterpolationHigh)
        [bg setImage:[img resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake([[AppData sharedAppData] screenWidth]*2, [[AppData sharedAppData] screenHeight]*2) interpolationQuality:quality]];
    else
        [bg setImage:[img resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake([[AppData sharedAppData] screenWidth], [[AppData sharedAppData] screenHeight]) interpolationQuality:quality]];
    

}

- (void)setBackgroundImage:(UIImage*)bgImage save:(bool)save{
   
    if(save)
        [self performSelectorInBackground:@selector(saveImageInBackground:) withObject:bgImage];
    
    [bg setContentMode:UIViewContentModeScaleAspectFill] ;
    int fifth = [[AppData sharedAppData] screenHeight]/5;
    if([bgImage size].width >= [bgImage size].height)
        [bg setFrame:CGRectMake(0,fifth, [[AppData sharedAppData] screenWidth], [[AppData sharedAppData] screenHeight]-(fifth<<1))];
    else
        [bg setFrame:CGRectMake(0,0,[[AppData sharedAppData] screenWidth],[[AppData sharedAppData] screenHeight])];
    
        
    /*
    //float iphone_default = 2.0/3.0; //0.666666...
    float imagewidth = bgImage.size.width;
    float imageheight = bgImage.size.height;
    
    float imageratio = imagewidth/imageheight;
    
    float zoomlevel;
    
    //printf("iphone %f : %f  image", iphone_default, imageratio);
    
    if(imageratio <= 0.8){
        zoomlevel = 960.0/imageheight;
        float z = 480.0/imageheight;
        [bg setFrame:CGRectMake(lroundf(320.0-imagewidth*z)/2, 0, imagewidth*z, 480)];
    }
    else{
        zoomlevel = 640.0/imagewidth *1.1;
        [bg setFrame:CGRectMake(-16, -24, 320+32, 480+48)];
    }
    
    //UIImage *newimg = [bgImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(imagewidth*zoomlevel,imageheight*zoomlevel) interpolationQuality:0.7];
    if(!resize){
        UIImage *newimg = [bgImage resizedImage:CGSizeMake((int)(imagewidth*zoomlevel),(int)(imageheight*zoomlevel)) interpolationQuality:0.7];
        [bg setImage:newimg];
    }
    else{
        [bg setImage:bgImage];
        
    }
     */

    [bg setImage:[bgImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake([[AppData sharedAppData] screenWidth], [[AppData sharedAppData] screenHeight]) interpolationQuality:kCGInterpolationLow]];

}


- (UIImage*)getBgImage{
    return [bg image];
}

- (void)setTitle:(NSString*)newTitle saveToDB:(bool)save{
    UILabel *title = (UILabel*)[panel viewWithTag:TITLE];
    [title setText:newTitle];

    title_ = newTitle;
    bIsTitleSet = true;
    if(save)
        [[AppData sharedAppData] db_updateTitle:self];
}



- (void)setDate:(NSDate*)date saveToDB:(bool)save{
    date_ = date;
    int days = [[AppData sharedAppData] daysBetweenDate:date andDate:[NSDate date]];
    if(days < 0){
        days = -days;
        bDdayPast = false;
    }
    else
        bDdayPast = true;
    
    eventDaysLeft = days;
    [self drawPanel];
    
    if(save)
        [[AppData sharedAppData] db_updateDate:self];
    
    bIsDateSet = true;
    
}
 

- (bool)isTitleSet{
    return bIsTitleSet;
}

- (bool)isDateSet{
    return bIsDateSet;
}

- (NSString*)getTitle{
    return title_;
}

- (NSDate*)getDate{
    return date_;
}

- (NSString*)getDateString{
    return [[AppData sharedAppData] dateToString:date_];
}

- (NSString*)getDateStringShort{
    return [[AppData sharedAppData] dateToStringShort:date_];
}
- (void)clearDate{
    bIsDateSet = false;
 //   [AppData db_clearDate:self];
}

- (void)clearTitle{
    bIsTitleSet = false;
 //   [AppData db_clearTitle:self];
}

- (bool)isAlarmEnabled{
    return bAlarmEnabled;
}

- (void)setAlarmEnabled:(bool)value{
    bAlarmEnabled = value;
}

- (NSInteger)getAlarmDays{
    return alarmDays_;
}

- (void)setAlarmDays:(NSInteger)value{
    alarmDays_ = value;
}

- (NSInteger) getAlarmBeforeOrAfter{
    return alarmBeforeOrAfter_;
}

- (void)setAlarmBeforeOrAfter:(NSInteger)value{
    alarmBeforeOrAfter_ = value;
}

- (NSDate*)getAlarmTime{
    return alarmTime_;
}

- (void)setAlarmTime:(NSDate *)value{
    alarmTime_ = value;
}

- (NSInteger) getUID{
    return uid;
}
- (void) setUID:(NSInteger)newUid{
    uid = newUid;
}


- (NSInteger) getEventDaysLeft{
    return eventDaysLeft;
}
- (bool) getDdayPast{
    return bDdayPast;
}

- (NSInteger) getEventDaysLeftOrPast{
    return eventDaysLeft*(bDdayPast?1:-1);
}


/*
- (void)switchPresetStrings:(id)sender {
    if ([(UISwitch *)sender isOn]) {
        multiDialView.presetStrings = [[NSArray alloc] initWithObjects:@"000A", @"111A", @"222B", @"333C", @"360D", nil];
    }
    else {
        multiDialView.presetStrings = nil;
    }
    self.presetStringsView.text = [NSString stringWithFormat:@"%@", multiDialController.presetStrings];
}
*/

- (void)multiDialViewController:(MultiDialViewController *)controller didSelectString:(NSString *)string{
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    //NSLog(@"before UIIMAGE %f  %f", image.size.width, image.size.height);

    // Create a bitmap context.
    UIGraphicsBeginImageContextWithOptions(newSize, YES, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //NSLog(@"after UIIMAGE %f  %f", image.size.width, image.size.height);
    return newImage;
}

- (void)saveImageInBackground:(UIImage*)img{
    if(uid == -1)
        return;
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatchQueue, ^(void) {
        // Your code here
    //    if([[AppData sharedAppData] writeToFile:img fileName:[NSString stringWithFormat:@"%d.jpeg", uid]] == true)
        bool needResize = false;
        bool portrait = [img size].height > [img size].width;
        if([img size].width > 1200 && [img size].height > 1200)
            needResize = true;
        
        if([[AppData sharedAppData] writeToFile:[img resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                                          bounds:needResize?(portrait?CGSizeMake(800, 1300):CGSizeMake(1300,800)):[img size]
                                                            interpolationQuality:kCGInterpolationHigh]
                                       fileName:[NSString stringWithFormat:@"%d.jpeg", uid]] == true)
            NSLog(@"WRITE SUCCESS");
        else
            NSLog(@"DELETE SUCCESS");
    });
}



@end
