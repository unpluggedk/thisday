//
//  SettingsViewController.m
//  ThisDate
//
//  Created by jason kim on 10/3/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppData.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

@synthesize delegate;

-(id)init{
    if((self=[super initWithStyle:UITableViewStyleGrouped])){
        self.title = @"Settings";
                //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
            }
    return self;
}

-(void)done:(id)sender{
    [delegate reloadPanels];
    [self dismissModalViewControllerAnimated:YES];
    // [delegate doneCompleted];
    
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];

    self.navigationItem.leftBarButtonItem = doneButton;

    keys_ = [[NSArray alloc] initWithObjects:@"Counting", @"Dropbox", @"Reset", nil];
    
    counting_ = [[NSArray alloc] initWithObjects:@"Count days from 1",  nil];
    dropbox_ = [[NSMutableArray alloc] initWithObjects:@"Link Dropbox", nil];//@"Sync settings", nil];
    reset_ = [[NSArray alloc] initWithObjects:@"Clear all events", nil];
    
    dataSource_ = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:counting_, dropbox_, reset_, nil]
                                                forKeys:keys_];
  
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [keys_ count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id key = [keys_ objectAtIndex:section];
    return [[dataSource_ objectForKey:key] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[3] = {@"counting-cell", @"dropbox-cell", @"reset-cell"};
    NSString *identifier = identifiers[0];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    // Configure the cell...
    if(cell == nil){
        switch(indexPath.section){
            case 0:{ // counting-cell
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
                UISwitch *countingSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
                [countingSwitch setOn:[[AppData sharedAppData] getCountFromOne]];
                [countingSwitch addTarget:self action:@selector(countingSwitchChanged:) forControlEvents:UIControlEventValueChanged];
                cell.accessoryView = countingSwitch;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                break;
            }
            case 1:{ // dropbox-cell
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];

                break;
            }
            case 2:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            default:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
    }
    
    id key = [keys_ objectAtIndex:indexPath.section];
    NSString *text = [[dataSource_ objectForKey:key] objectAtIndex:indexPath.row];
    cell.textLabel.text = text;
  
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            if([[AppData sharedAppData] isDropboxLinked]){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.detailTextLabel.text = @"Dropbox is currently linked";
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.detailTextLabel.text = @"Dropbox is not linked";
            }
        }
        else if(indexPath.row == 1){
            cell.detailTextLabel.text = @"Change Fetching Intervals";
        }
    }
    
    return cell;
}

- (void)countingSwitchChanged:(id)sender{
    UISwitch* switchControl = sender;
    [[AppData sharedAppData] setCountFromOne:switchControl.on];
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
*/
- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [keys_ objectAtIndex:section];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    if(section == 0) //Counting
        return @"For past events, count the event day";

    return @"";
            
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2){ //reset
        [self resetActionSheet];
    }
    
    else if(indexPath.section == 1 && indexPath.row == 0){
        if([[AppData sharedAppData] isDropboxLinked]){
            NSLog(@"UNLINK!");
            [[AppData sharedAppData] unlinkDropbox];
            [self.tableView reloadData];
        }
        else{
            NSLog(@"LINK");
            [[AppData sharedAppData] linkDropBoxWith:self];
        }
     
      
    }
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dropBoxLinkCompleted:(bool)result{
    NSLog(@"  **  link complete : %s ** ", result?"ON":"OFF");
    [self updateLinkStatus:result];
}

- (void)updateLinkStatus:(bool)result{
    NSLog(@"UNLINK ---");
    if(result == true){
        [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].accessoryType = UITableViewCellAccessoryCheckmark;
        [dropbox_ removeObjectAtIndex:0];
        [dropbox_ insertObject:@"Unlink Dropbox" atIndex:0];
    }
    else
        [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].accessoryType = UITableViewCellAccessoryNone;
    [self.tableView reloadData];
}

- (void)resetActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Reset"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Clear All Events"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 1;
    sheet.destructiveButtonIndex = 0;
    [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [delegate reset];
    }
}

@end
