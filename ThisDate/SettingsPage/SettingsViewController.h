//
//  SettingsViewController.h
//  ThisDate
//
//  Created by jason kim on 10/3/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SettingsViewControllerDelegate
- (void) reset;
- (void) reloadPanels;
@end

@interface SettingsViewController : UITableViewController <UIActionSheetDelegate>{
    NSArray *keys_;
    NSArray *counting_;
    NSMutableArray *dropbox_;
    NSArray *reset_;
    
    NSDictionary *dataSource_;
}

@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;

- (void)dropBoxLinkCompleted:(bool)result;

@end
