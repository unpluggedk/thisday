//
//  AppDelegate.h
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DropboxSDK/DropboxSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>{
    DBSession *dbSession;
}

@property (strong, nonatomic) UIWindow *window;

@end
