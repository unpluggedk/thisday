//
//  MainViewController.h
//  ThisDate
//
//  Created by jason kim on 12/2/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate>

- (IBAction)showInfo:(id)sender;

@end
