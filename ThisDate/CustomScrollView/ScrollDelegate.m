//
//  ScrollDelegate.m
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "ScrollDelegate.h"

@interface ScrollDelegate ()

@end

@implementation ScrollDelegate

@synthesize pageControl;
@synthesize singlePageHeight;
@synthesize singlePageWidth;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPageController:(UIPageControl *)pageController
{
    self = [super init];
    singlePageHeight = 0;
    singlePageWidth  = 0;
    
    pageControl = pageController;
    
    return self;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (scrollView.contentOffset.y != 0) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    }

    CGFloat pageWidth = scrollView.frame.size.width;
    pageControl.currentPage = floor((scrollView.contentOffset.x - pageWidth/3) / pageWidth) +1;

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [delegate scrollViewDidEndDecelerating];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [delegate scrollViewWillBeginDragging];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if(singlePageHeight == 0 && singlePageWidth == 0)
        return;
    else
        [scrollView setContentSize:CGSizeMake(singlePageWidth * pageControl.numberOfPages, singlePageHeight)];

}

@end
