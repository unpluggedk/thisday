//
//  AppScrollView.h
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>

// custom uiscrollview
// handles dragging only.
// other events are passed on to next reposnder in the chainß
@interface AppScrollView : UIScrollView
-(id)initWithFrame:(CGRect)frame;
@end