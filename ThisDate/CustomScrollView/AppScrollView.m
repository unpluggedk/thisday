//
//  AppScrollView.m
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "AppScrollView.h"

@implementation AppScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // If not dragging, send event to next responder
    if(!self.dragging){
        [[self.nextResponder nextResponder] touchesEnded:touches withEvent:event];
    }
    else{
        [super touchesEnded:touches withEvent:event];
    }
}
@end
