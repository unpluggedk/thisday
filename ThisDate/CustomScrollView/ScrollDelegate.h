//
//  ScrollDelegate.h
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomScrollViewDelegate
- (void) scrollViewDidEndDecelerating;
- (void) scrollViewWillBeginDragging;
@end


@interface ScrollDelegate : UIViewController <UIScrollViewDelegate>{
    UIPageControl *pageControl;
    NSInteger singlePageWidth;
    NSInteger singlePageHeight;
}

-(id)initWithPageController:(UIPageControl *)pageController;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, assign) NSInteger singlePageWidth;
@property (nonatomic, assign) NSInteger singlePageHeight;


@property (nonatomic, weak) id<CustomScrollViewDelegate> delegate;


@end
