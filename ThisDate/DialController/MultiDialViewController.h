//
//  Created by Dimitris Doukas on 25/03/2011.
//  Copyright 2011 doukasd.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialController.h"

@protocol MultiDialViewControllerDelegate;

@interface MultiDialViewController : UIViewController  <UIAccelerometerDelegate, DialControllerDelegate> {
    id<MultiDialViewControllerDelegate> __unsafe_unretained delegate;
    
    DialController *dial1;
    DialController *dial2;
    DialController *dial3;
    DialController *dial4;
    
    //optional array of preset values
	NSMutableArray *presetStrings;
}

@property (nonatomic, unsafe_unretained) id<MultiDialViewControllerDelegate> delegate;

@property (nonatomic, strong) DialController *dial1;
@property (nonatomic, strong) DialController *dial2;
@property (nonatomic, strong) DialController *dial3;
@property (nonatomic, strong) DialController *dial4;

@property (nonatomic, strong) NSMutableArray *presetStrings;

- (void)spinToRandomString:(BOOL)preset;

@end


@protocol MultiDialViewControllerDelegate 
- (void)multiDialViewController:(MultiDialViewController *)controller didSelectString:(NSString *)string;
@end