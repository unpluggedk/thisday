//
//  InfoPageViewController.m
//  ThisDate
//
//  Created by jason kim on 8/29/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "InfoPageViewController.h"
#import "TopPageViewController.h"

@interface InfoPageViewController ()

@end


@implementation InfoPageViewController
@synthesize delegate;


-(id)init{
    if((self=[super initWithStyle:UITableViewStyleGrouped])){
        self.title = @"Infopage";
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
        //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
        self.navigationItem.leftBarButtonItem = doneButton;
    }
    return self;
}

-(void)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
   // [delegate doneCompleted];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [delegate doneCompleted];
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}




- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    // Data fillup
    keys_ = [[NSArray alloc] initWithObjects:@"Image", @"Title & Date", @"Alarm", nil];
    
    bg = [NSArray arrayWithObjects:@"Change Image", nil];
    title_date = [NSArray arrayWithObjects:@"Title", @"Date", nil];
    alarm = [NSMutableArray arrayWithObjects:@"Alarm", nil];
    
    objects = [NSArray arrayWithObjects:bg, title_date, alarm, nil];
    
    dataSource_ = [[NSDictionary alloc] initWithObjects:objects forKeys:keys_];
    
    if([delegate isAlarmSet]){
        [alarm addObject:@"date"];
        [alarm addObject:@"time"];
    }
    
    

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [keys_ count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id key = [keys_ objectAtIndex:section];
    return [[dataSource_ objectForKey:key] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[3] = {@"image-cell", @"info-cell", @"alarm-cell"};
    NSString *identifier = identifiers[indexPath.section];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    // Configure the cell...
    
    if(cell == nil){
        switch (indexPath.section) {
            case 0: //image-cell
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            case 1: //info-cell (title & date)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
                break;
            case 2:{ //alarm
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
                if(indexPath.row == 0){
                    UISwitch *alarmSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
                    [alarmSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                    cell.accessoryView = alarmSwitch;
                    [alarmSwitch setOn:[delegate isAlarmSet] animated:NO];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                
                
            }break;
            default:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
                break;
        }
    }
    
    id key = [keys_ objectAtIndex:indexPath.section];
    NSString *text = [[dataSource_ objectForKey:key] objectAtIndex:indexPath.row];
    
    switch (indexPath.section) {
        case 0: {//image-cell
            cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = text;
            NSLog(@"cell font => %@",cell.textLabel.font);

        }   break;
        case 1:{
            cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
            if(indexPath.row == 0)
                cell.detailTextLabel.text = [delegate getTitle];
            else{
                if([delegate isDateSet])
                    cell.detailTextLabel.text = [delegate getDateStr];
                else
                    cell.detailTextLabel.text = @"Set Date";
            }
            cell.textLabel.text = text;
        }
            break;
        case 2:{
            cell.textLabel.text = text;
            if(indexPath.row == 0){
                
                ;
            }
            else if(indexPath.row == 1){
                cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
                int days = [delegate getAlarmDays];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%d days %@", (days<0?-days:days), days<0?@"before":@"after"];
            }
            else{
                //cell.accessoryView = nil;
                cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
                cell.detailTextLabel.text = [[AppData sharedAppData] dateToStringHM:[delegate getAlarmTime]];
            }
        }
            break;
        default:
            cell.textLabel.text = text;
            break;
    }
    return cell;
}


- (void) switchChanged:(id)sender {
    UISwitch* switchControl = sender;
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:2];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
    
    if(switchControl.on){
        [delegate updateAlarmSet:true];
        [alarm addObject:@"date"];
        [alarm addObject:@"time"];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1, indexPath2, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView scrollToRowAtIndexPath:indexPath2 atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    else{
        [delegate updateAlarmSet:false];
        
        if([alarm count] == 1)
            return;
        [alarm removeObjectAtIndex:2];
        [alarm removeObjectAtIndex:1];
        
        //    [self.tableView beginUpdates];
        //[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath2, indexPath1, nil] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath2, indexPath1, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        //  [self.tableView endUpdates];
        //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationLeft];
        
    }
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}


- (void)viewWillAppear:(BOOL)animated{
    self.title = @"Infopage";
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        if(![[alertView textFieldAtIndex:0] hasText]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Enter a title" delegate:nil cancelButtonTitle:@"Confirm" otherButtonTitles:nil];
            [alert show];
        }
        else
            [self updateTitle:[alertView textFieldAtIndex:0].text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [titleView dismissWithClickedButtonIndex:titleView.firstOtherButtonIndex animated:YES];
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        [self addPageActionSheet];
    }
    if(indexPath.section == 1 && indexPath.row == 0){
        
        /*
        EnterTitleViewController *enterTitleVC = [[EnterTitleViewController alloc] init];
        [enterTitleVC setDelegate:self];
        if([delegate isTitleSet]){
            [enterTitleVC updateTitle:[delegate getTitle]];
        }
        self.title = @"cancel";
        [self.navigationController pushViewController:enterTitleVC animated:YES];
        //UINavigationController *navigationController = [[UINavigationController alloc]
        //                                              initWithRootViewController:enterTitleVC];
        
        //[self presentModalViewController:navigationController animated:YES];
         */
        titleView = [[UIAlertView alloc] initWithTitle:@"Enter a title"
                                                            message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        titleView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [titleView textFieldAtIndex:0].placeholder = @"Enter a title";
        [titleView textFieldAtIndex:0].delegate = self;
        if([delegate isTitleSet])
            [titleView textFieldAtIndex:0].text = [delegate getTitle];
        [titleView show];
    }
    else if(indexPath.section == 1 && indexPath.row == 1){ //date
        if(_dateActionSheetPicker == nil){
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            NSDateComponents *comp = [[NSDateComponents alloc] init];
            [comp setDay:0];
            [comp setMonth:0];
            [comp setYear:-25];

            NSDate *minDate = [calendar dateByAddingComponents:comp toDate:[NSDate date] options:0];
            
            [comp setYear:25];
            NSDate *maxDate = [calendar dateByAddingComponents:comp toDate:[NSDate date] options:0];
            
            _dateActionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@""
                                                             datePickerMode:UIDatePickerModeDate
                                                               selectedDate:[NSDate date]
                                                                     target:self
                                                                     action:@selector(dateWasSelected:element:)
                                                                     origin:self.view
                                                                          minDate:minDate maxDate:maxDate];
            _dateActionSheetPicker.hideCancel = NO;
        }

        if([delegate isDateSet])
            [_dateActionSheetPicker selectedDate:[delegate getDate]];
        else
            [_dateActionSheetPicker selectedDate:[NSDate date]];
        [_dateActionSheetPicker showActionSheetPicker];
    }
    
    else if(indexPath.section == 2 && indexPath.row == 1){ // alarm - date
        
        if(_daysActionSheetPicker == nil){
            ActionDaysDoneBlock done = ^(ActionSheetDaysPicker *picker, NSInteger selectedIndex_days, NSInteger selectedIndex_beforeOrAfter, id selectedValue_days, id selectedValue_beforeOrAfter){
                [delegate updateAlarmDays:selectedIndex_days * (selectedIndex_beforeOrAfter==0?-1:1)];
                [self.tableView reloadData];
            };
            
            ActionDaysCancelBlock   cancel = ^(ActionSheetDaysPicker *picker){
                ;
            };
            
            _daysActionSheetPicker = [[ActionSheetDaysPicker alloc] initWithTitle:@"Days from event"
                                                                                                    from:0
                                                                                                    till:365
                                                                                        initialSelection:0
                                                                                               doneBlock:done
                                                                                             cancelBlock:cancel
                                                                                                  origin:self.view];
            
           // [delegate updateAlarmBeforeOrAfter:0];
           // [delegate updateAlarmDays:0];
        }
        int days = [delegate getAlarmDays];
        [_daysActionSheetPicker selectedDays:days<0?-days:days beforeOrAfter:days<0?0:1];
        [_daysActionSheetPicker showActionSheetPicker];
    }
    
    else if(indexPath.section == 2 && indexPath.row == 2){ // alarm - time
        if(_alarmTimeActionSheetPicker == nil){
            _alarmTimeActionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@""
                                                                        datePickerMode:UIDatePickerModeTime
                                                                          selectedDate:[NSDate date]
                                                                                target:self
                                                                                action:@selector(timeWasSelected:element:)
                                                                                origin:self.view];
            _alarmTimeActionSheetPicker.hideCancel = NO;
            //[delegate updateAlarmTime:[[AppData sharedAppData] dateWithZeroHMS:[NSDate date]]];
        }
        [_alarmTimeActionSheetPicker selectedDate:[delegate getAlarmTime]];
        [_alarmTimeActionSheetPicker showActionSheetPicker];
    }
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    [delegate updateDate:[[AppData sharedAppData] dateWithZeroHMS:selectedDate]];
    [self.tableView reloadData];
    
}


- (void)timeWasSelected:(NSDate *)selectedDate element:(id)element {
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    [delegate updateAlarmTime:selectedDate];
    [self.tableView reloadData];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [keys_ objectAtIndex:section];
}

- (void)addPageActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Choose a background"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Camera"];
    [sheet addButtonWithTitle:@"Photos"];
    [sheet addButtonWithTitle:@"Random"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 3;
    [sheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex)
        return;
    else if(buttonIndex == 0){
        // camera
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentModalViewController:picker animated:YES];
        }
    }
    else if(buttonIndex == 1){
        // photo
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentModalViewController:picker animated:YES];
        }
    }
    else{
        // random
        int r = rand() % 2;
        NSLog(@"rand = %d", r);
        UIImage *img;
        if(r==0){
            img = [UIImage imageNamed:@"img.JPG"];
        }
        else{
            img = [UIImage imageNamed:@"medium2.png"];
        }
        [delegate updateImage:img];
        
    }
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [delegate updateImage:image];
    [self dismissModalViewControllerAnimated:YES];
    
}

- (void)updateTitle:(NSString *)title{
    NSLog(@"InfoPageViewController setTitle");
    [delegate updateTitle:title];
    [self.tableView reloadData];
}

- (NSString *)getTitle{
    return [delegate getTitle];
}

/*
- (void)changeDate:(UIDatePicker *)sender {
    //   NSLog(@"New Date: %@", sender.date);
}
*/


@end