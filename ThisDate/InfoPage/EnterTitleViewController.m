//
//  EnterTitleViewController.m
//  ThisDate
//
//  Created by jason kim on 9/6/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//


#import "EnterTitleViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface EnterTitleViewController ()

@end

@implementation EnterTitleViewController

@synthesize delegate;
@synthesize titleTextfield;

-(id)init{
    if((self=[super init])){
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
        
    //    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc ] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
        self.navigationItem.rightBarButtonItem = doneButton;
        titleText = @"";
        self.navigationItem.title = @"Title";
    }
    return self;
}

-(void)done:(id)sender{
    if([titleTextfield.text length] == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Enter a title" delegate:self cancelButtonTitle:@"Confirm" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [delegate updateTitle:titleTextfield.text];
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // deprecated [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    
    UITableView *bg = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStyleGrouped];
    [self.view addSubview:bg];
    [self.view setBackgroundColor:[UIColor colorWithRed:215.0/255.0 green:217.0/255.0 blue:223.0/255.0 alpha:1.0]];
    titleTextfield = [[UITextField alloc] initWithFrame:CGRectMake(20, 70, 280, 40)];
    [titleTextfield setBorderStyle:UITextBorderStyleRoundedRect];
    [titleTextfield setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [titleTextfield setBackgroundColor:[UIColor whiteColor]];
    [titleTextfield setText:titleText];
    [titleTextfield setPlaceholder:@"Enter a title"];
    [titleTextfield setClearButtonMode:UITextFieldViewModeAlways];
    [self.view addSubview:titleTextfield];
    
    [titleTextfield becomeFirstResponder];
}

- (void)updateTitle:(NSString*)newTitle{
    titleText = newTitle;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
