//
//  InfoPageViewController.h
//  ThisDate
//
//  Created by jason kim on 8/29/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnterTitleViewController.h"
#import "../ActionSheetPicker/ActionSheetPicker.h"

@protocol InfoPageViewControllerDelegate
- (void) updateImage:(UIImage*)newImage;
- (void) updateTitle:(NSString*)newTitle;
- (void) updateDate:(NSDate*)newDate;
- (NSString*) getTitle;
- (bool)isDateSet;
- (NSDate*) getDate;
- (NSString*) getDateStr;
- (bool)isTitleSet;

- (bool)isAlarmSet;

- (void)updateAlarmSet:(bool)value;
- (NSInteger) getAlarmDays;
- (void) updateAlarmDays:(NSInteger)days;
//- (NSInteger) getAlarmBeforeOrAfter;
//- (void) updateAlarmBeforeOrAfter:(NSInteger)value;
- (NSDate*) getAlarmTime;
- (void) updateAlarmTime:(NSDate*)time;

- (void) doneCompleted;

@end

@interface InfoPageViewController : UITableViewController <UINavigationControllerDelegate ,UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, EnterTitleViewControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate> {
    NSArray *keys_;
    NSDictionary *dataSource_;

    NSArray *bg;
    NSArray *title_date;
    NSMutableArray *alarm;
    
    NSArray *objects;
    
    ActionSheetDatePicker *_dateActionSheetPicker;
    ActionSheetDaysPicker *_daysActionSheetPicker;

    ActionSheetDatePicker *_alarmTimeActionSheetPicker;
    UIAlertView *titleView;
    //UISwitch
    //id<InfoPageViewControllerDelegate> delegate;
}

@property (nonatomic, weak) id<InfoPageViewControllerDelegate> delegate;

@end


