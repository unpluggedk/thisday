//
//  EnterTitleViewController.h
//  ThisDate
//
//  Created by jason kim on 9/6/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EnterTitleViewControllerDelegate
- (void)updateTitle:(NSString*)newTitle;
@end

@interface EnterTitleViewController : UIViewController{
    UITextField *titleTextfield;
    NSString *titleText;
}

@property(nonatomic, retain) UITextField *titleTextfield;
@property (nonatomic, weak) id<EnterTitleViewControllerDelegate> delegate;

- (void) updateTitle:(NSString*)newTitle;


@end


