//
//  TopPageListviewCell.m
//  ThisDate
//
//  Created by jason kim on 9/15/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "TopPageListviewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TopPageListviewCell
@synthesize image_;
@synthesize titleLabel;
@synthesize topLeftLabel;
@synthesize topRightLabel;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        //image
        image_ = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,60,60)];
        [image_ setContentMode:UIViewContentModeScaleAspectFill];
        [image_ setClipsToBounds:YES];
        [image_.layer setCornerRadius:3.0f];
        //[image_ setImage:[UIImage imageNamed:@"d1.png"]];
        [self.contentView addSubview:image_];
        
        topLeftLabel = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(70, 10, 130, 20)];
        [topLeftLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:topLeftLabel];
        
        topRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(200,10, 80, 20)];
        [topRightLabel setTextAlignment:UITextAlignmentRight];
        [topRightLabel setTextColor:[UIColor lightGrayColor]];
        [topRightLabel setFont:[UIFont systemFontOfSize:13]];
        [topRightLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:topRightLabel];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 200, 25)];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:titleLabel];

        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
    [super layoutSubviews];
    //    [[self contentView] frame
}

@end
