//
//  AppDelegate.m
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "AppDelegate.h"
#import "TopPageViewController.h"
#import "AppData.h"

@implementation AppDelegate

TopPageViewController *rootController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"didFinishLaunch...");
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    rootController = [[TopPageViewController alloc] initWithNotification:localNotification];
    
    rootController.wantsFullScreenLayout = YES;
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:rootController];
    
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    dbSession = [[DBSession alloc] initWithAppKey:@"li0qvefq5ilh7ax"
                                        appSecret:@"ki734uffthd9m0p" root:kDBRootAppFolder];
    
    [DBSession setSharedSession:dbSession];

// todo - multiple expired notificationslocalNotification
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"RESIGN FROM ACTIVE");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"Enterbackground");
    [[NSUserDefaults standardUserDefaults] setInteger:[rootController getCurrentPage] forKey:@"lastpage"];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    // to-do: if badge > 0
    //        jump to today page
    //        remove all badges
 //   if([[AppData sharedAppData] badgeCount] > 0){
 //       [[AppData sharedAppData] clearBadgeCount];
        /*
         UIAlertView *alert;
            alert = [[UIAlertView alloc] initWithTitle:@"Event alarm in waiting"
                                               message:@"Please check notifications for more information" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Delete", nil];
        [alert show];
        */
//    }
    NSLog(@"ENTER FOREGROUND");

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"clicked %s", __func__);
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
        NSLog(@"clicked %s", __func__);
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
        NSLog(@"clicked %s", __func__);
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"BECAME ACTIVE   count %d", [[[UIApplication sharedApplication] scheduledLocalNotifications] count]);

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"TERMINATE");
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
   
    NSLog(@"DIDRECEIVELOCALNOTI");
    [rootController notificationHandler:notification];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
            NSLog(@"App linked successfully!");
            // At this point you can start making API calls
            [[AppData sharedAppData] linkCompleted:true];
        }
        else
            [[AppData sharedAppData] linkCompleted:false];
        return YES;
    }
    // Add whatever other url handling code your app requires here
    return NO;
}

@end
