//
//  AppData.m
//  ThisDate
//
//  Created by jason kim on 8/16/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "AppData.h"
static AppData *sharedAppData = nil;

static NSDateFormatter *format;
static NSDateFormatter *format_short;
static NSDateFormatter *format_hm;
static NSDateFormatter *format_full; //2013/12/31 01:59 AM
//static NSDateFormatter *format_sql_date; //yyyy-MM-dd
//static NSDateFormatter *format_sql_full; //yyyy-MM-dd HH:mm:ss Z
static sqlite3 *databaseHandle; 
static NSFileManager *fileManager;
//static int SCREENWIDTH;
//static int SCREENHEIGHT;

@implementation AppData

@synthesize entries;
@synthesize sortedIndices;

@synthesize screenWidth;
@synthesize screenHeight;
@synthesize delegate;

#pragma mark - Initialization
+ (id)sharedAppData{
    @synchronized(self){
        if(sharedAppData == nil){
            Class notificationClass = NSClassFromString(@"UILocalNotification");
            if(notificationClass == nil)
                sharedAppData = nil;
            else{
                sharedAppData = [[super allocWithZone:NULL] init];

                fileManager = [NSFileManager defaultManager];
 ;

                //sharedAppData.countFromOne = [[NSUserDefaults standardUserDefaults] boolForKey:@"countFromOne"];
                //sharedAppData.dropboxLinked = [[NSUserDefaults standardUserDefaults] boolForKey:@"dropboxLinked"];
            }
        }
    }
    return sharedAppData;
}


- (id)init{
    self = [super init];
    if(self){
        // initialize entries
        entries = [[NSMutableArray alloc] initWithCapacity:MAXNUMENTRIES];
        NSTimeZone *currentTimeZone = [NSTimeZone defaultTimeZone];
        format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"MMM dd, yyyy"];
        [format setTimeZone:currentTimeZone];
        
        format_hm = [[NSDateFormatter alloc] init];
        [format_hm setDateFormat:@"hh:mm a"];
        [format_hm setTimeZone:currentTimeZone];
        
        format_short = [[NSDateFormatter alloc] init];
        [format_short setDateFormat:@"yyyy/MM/dd"];
        [format_short setTimeZone:currentTimeZone];
        
        format_full = [[NSDateFormatter alloc] init];
        [format_full setDateFormat:@"yyyy/MM/dd hh:mm a"];
        [format_full setTimeZone:currentTimeZone];
        
        [self initDatabase];
        //[self restoreAtInit];
    }
    NSLog(@"APP DATA INITIALIZED");
    return self;
    
}

- (void)dealloc{
    sqlite3_close(databaseHandle);
}



#pragma mark - Count from one
- (void)setCountFromOne:(bool)newVal{
    [[NSUserDefaults standardUserDefaults] setBool:newVal forKey:@"countFromOne"];
}
- (bool)getCountFromOne{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"countFromOne"];
}

#pragma mark - Dropbox

UIViewController *dropBoxViewController;
- (void)linkDropBoxWith:(UIViewController*)viewController{
    dropBoxViewController = viewController;
  //  if (![[DBSession sharedSession] isLinked]) {
        [[DBSession sharedSession] linkFromController:viewController];
  //  }
  //  else
  //      NSLog(@"DBox already linked");
}

- (void)linkCompleted:(bool)result{
    NSLog(@"LINK DONE");
    [(SettingsViewController*)dropBoxViewController dropBoxLinkCompleted:result];
    [[NSUserDefaults standardUserDefaults] setBool:result forKey:@"dropboxLinked"];
}

- (void)unlinkDropbox{
    [[DBSession sharedSession] unlinkAll];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"dropboxLinked"];
}
- (bool)isDropboxLinked{
    //return [[NSUserDefaults standardUserDefaults] boolForKey:@"dropboxLinked"];
    return [[DBSession sharedSession] isLinked];
}

- (DBRestClient *)restClient {
    if (!restClient) {
        restClient =
        [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata {
    
    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    [delegate updateCompleted:true];
}

- (void)restClient:(DBRestClient *)client uploadProgress:(CGFloat)progress forFile:(NSString *)destPath from:(NSString *)srcPath{
    NSLog(@"%.2f", progress);
    [delegate updatePanelProgress:progress];
}

- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    NSLog(@"File upload failed with error - %@", error);
    [delegate updateCompleted:false];
}



#pragma mark - Notification
- (void) scheduleNotificationOn:(NSDate*) fireDate
						   text:(NSString*) alertText
						 action:(NSString*) alertAction
						  sound:(NSString*) soundfileName
					launchImage:(NSString*) launchImage
						andInfo:(NSDictionary*) userInfo{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = fireDate;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = alertText;
    localNotification.alertAction = alertAction;
    
    if(soundfileName == nil)
        localNotification.soundName = UILocalNotificationDefaultSoundName;
    else
        localNotification.soundName = soundfileName;
    
    localNotification.alertLaunchImage = launchImage;

    localNotification.userInfo = userInfo;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}




- (void) handleReceivedNotification:(UILocalNotification*) thisNotification
{
    [[UIApplication sharedApplication] cancelLocalNotification:thisNotification];
	NSLog(@"Received: %@",[thisNotification description]);

}

- (void)deleteNotification:(SinglePage *)page{
    NSArray *alarms = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for(UILocalNotification *noti in alarms){
        NSLog(@"looking for %d,   looking at %d", [page getUID], [[[noti userInfo] objectForKey:@"uid"] intValue]);
        if( [[[noti userInfo] objectForKey:@"uid"] intValue] == [page getUID]  ){
            NSLog(@"FOUND");
            [[UIApplication sharedApplication] cancelLocalNotification:noti];
        }
    }
}

- (void) updateNotification:(SinglePage *)page{
    NSLog(@"alarm off");
    [self deleteNotification:page];
    
    if([page bAlarmEnabled]){ // alarm on
        NSLog(@"alarm on");
        NSTimeInterval timeInterval = [page getAlarmDays] * 24 * 60 * 60;
        NSDate *alarmDate = [NSDate dateWithTimeInterval:timeInterval sinceDate:[page getDate]];
        NSString *alarmStr = [NSString stringWithFormat:@"%@ %@", [self dateToStringShort:alarmDate], [self dateToStringHM:[page getAlarmTime]]];
        NSDate *alarmDateTime = [format_full dateFromString:alarmStr];
        if([alarmDateTime timeIntervalSinceNow] <= 0)
            return;
        NSDictionary *info = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObject:[NSNumber numberWithInt:[page getUID]]]
                                                           forKeys:[NSArray arrayWithObject:@"uid"]];
        [self scheduleNotificationOn:[format_full dateFromString:alarmStr]
                                text:[page getTitle] action:@"View" sound:nil launchImage:nil andInfo:info];

        
    }
}

#pragma mark - Days between date
- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}



#pragma mark - Common control
- (void)removeEntryAtPage:(int)pageNum{
    if(pageNum < [entries count]){
        SinglePage *page = (SinglePage*)[entries objectAtIndex:pageNum];
        [self deleteNotification:page];
        [self db_deletePage:page];
        [self db_order_removePage:pageNum];
        [entries removeObjectAtIndex:pageNum];
        
        [self deleteFile:[NSString stringWithFormat:@"%d.jpeg", [page getUID]]];
    

    }
}

- (bool)pre_addEntry:(SinglePage*)entry atPage:(int)pageNum{
    if([entries count] < MAXNUMENTRIES){
        [self db_addPage:entry];
        [self db_order_addPageWithUID:[entry getUID] atOrder:pageNum];
        return true;
    }
    return false;
    
}

- (bool)addEntry:(SinglePage*)entry atPage:(int)pageNum{
    if([entries count] < MAXNUMENTRIES){
        [entries addObject:entry];
//        [self db_addPage:entry];
//        [self db_order_addPageWithUID:[entry getUID] atOrder:pageNum];
       // [entry saveImageInBackground];
        return true;
    }
    return false;
}


- (void)moveEntryAtPage:(int)fromPage toPage:(int)destinationPage{
    int current = fromPage;
    int dir;
    if(fromPage < destinationPage)
        dir = 1;
    else
        dir = -1;
    while(current != destinationPage){
        [entries exchangeObjectAtIndex:current withObjectAtIndex:(current + dir)];
        current += dir;
    }
    [self db_order_movePageWithUID:[self db_order_getUidAtPage:fromPage] from:fromPage to:destinationPage];
}


- (SinglePage*)getPageAt:(int)targetPage{
    return targetPage<[entries count]?[entries objectAtIndex:targetPage]:NULL;
}

- (SinglePage*)getPageOfUID:(int)uid{
    for(int i=0; i< [entries count];i++){
        SinglePage *p = [entries objectAtIndex:i];
        if([p getUID] == uid)
            return p;
    }
    return nil;
        
}

- (int)getPageOrderOfUID:(int)uid{
    for(int i=0; i< [entries count];i++){
        if([[entries objectAtIndex:i] getUID] == uid)
            return i;
    }
    return -1;
    
}


- (int)getNumEntries{
    return [entries count];
}


- (NSString*)dateToString:(NSDate*)date{
    return [format stringFromDate:date];
}

- (NSString*)dateToStringShort:(NSDate*)date{
    return [format_short stringFromDate:date];
}

- (NSDate*)stringToDate:(NSString*)str{
    return [format dateFromString:str];
}
- (NSDate*)string_short_ToDate:(NSString*)str{
    return [format_short dateFromString:str];
}
- (NSDate*)string_hm_ToDate:(NSString*)str{
    return [format_hm dateFromString:str];
}

- (NSDate*)dateWithZeroHMS:(NSDate*)date{
    NSDate *boundary;
    [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit startDate:&boundary interval:NULL forDate:date];
    return boundary;}

- (NSString*)dateToStringHM:(NSDate*)date{
    return [format_hm stringFromDate:date];
}

- (void)togglePanelAlphaAtPage:(int)pageNum{
    SinglePage *page = [entries objectAtIndex:pageNum];
    if([page panelColor] == 0){
        [page setPanelColor:1];
    }
    else{
        [page setPanelColor:0];
    }
    [self db_updatePanelColor:page];
}

/*
- (NSDate*)boundaryForCalendarUnit:(NSCalendarUnit)calendarUnit
{
    NSDate *boundary;
    [[NSCalendar currentCalendar] rangeOfUnit:calendarUnit startDate:&boundary interval:NULL forDate:self];
    return boundary;
}

- (NSDate*)dayBoundary
{
    return [self boundaryForCalendarUnit:NSDayCalendarUnit];
}
*/
#pragma mark - Sort
- (void)sort{
    NSMutableArray *unsortedIndices = [[NSMutableArray alloc] initWithCapacity:MAXNUMENTRIES];
      for(int i=0; i<[entries count]; i++){
        SinglePageIndexData *p = [[SinglePageIndexData alloc] init];
        [p setIndex:i];
        [p setDays:[((SinglePage*)[entries objectAtIndex:i]) getEventDaysLeftOrPast]];
        [unsortedIndices addObject:p];
    }
    
    if(sortedIndices != nil){
        sortedIndices = nil;
    }
    
    if(sortedIndices)
        sortedIndices = nil;
    
    sortedIndices = [unsortedIndices sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if([((SinglePageIndexData*)obj1) days] < [((SinglePageIndexData*)obj2) days])
            return (NSComparisonResult)NSOrderedAscending;
        else if([((SinglePageIndexData*)obj1) days] > [((SinglePageIndexData*)obj2) days])
            return (NSComparisonResult)NSOrderedDescending;
        return (NSComparisonResult)NSOrderedSame;
    }];

    [unsortedIndices removeAllObjects];
    unsortedIndices = nil;
}

- (int)getOrderAtPage:(int)page{
    return [[sortedIndices objectAtIndex:page] index];
}

- (void)sortCompleted{
    NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:MAXNUMENTRIES];
    for(int i=0; i<[entries count]; i++){
        SinglePage *pageToAdd = [entries objectAtIndex: [[sortedIndices objectAtIndex:i] index]];
        [newArray addObject:pageToAdd];
        [self db_order_updatePageNumWithUID:[pageToAdd getUID] atPage:i];
    }
    
    [entries removeAllObjects];
    entries = newArray;
    
    newArray = nil;
    
}


#pragma mark - File IO
//Store Image/Songs files to Application Directory
//+(BOOL)writeToFile:(NSData *)data fileName:(NSString *)fileName {
-(BOOL)writeToFile:(UIImage *)image fileName:(NSString *)fileName{
    NSData* data = UIImageJPEGRepresentation(image, 0.7);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // the path to write file
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    return [data writeToFile:appFile atomically:YES];
   // return [fileManager createFileAtPath:appFile contents:data attributes:NULL];
}
//Image/songs -  Retrieve from Application Directory
-(NSData *)readFromFile:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    if (myData) {
        return myData;
    }
    return nil;
}

-(BOOL)deleteFile:(NSString*) fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    return [fileManager removeItemAtPath:filePath error:NULL];
}

#pragma mark - SQL
//=========================================
//   SQL
//=========================================
// TABLE PAGE (ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGEPATH TEXT, XCOORD INT, YCOORD INT, TITLE TEXT, TITLESET INT, EVENTDATE TEXT, EVENTDATESET INT, PANELCOLOR INT)
// TABLE ALARM (ID INTEGER PRIMARY KEY AUTOINCREMENT, ALARMENABLED INT, ALARMDAYS INT, ALARMTIME TEXT, PAGEID INT, FOREIGN KEY(PAGEID) REFERENCES PAGE(ID))
// TABLE IF NOT EXISTS PAGEORDER (ID INTEGER PRIMARY KEY, PAGENUM INT)

-(void)initDatabase
{
    // Create a string containing the full path to the sqlite.db inside the documents folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"sqlite.db"];
    
    // Check to see if the database file already exists
    bool databaseAlreadyExists = [[NSFileManager defaultManager] fileExistsAtPath:databasePath];
    
    // Open the database and store the handle as a data member
    if (sqlite3_open([databasePath UTF8String], &databaseHandle) == SQLITE_OK)
    {
        // Create the database if it doesn't yet exists in the file system
        if (!databaseAlreadyExists)
        {
            const char *sqlStatement;
            char *error;
            // Create the ENTRY table
            sqlStatement = "CREATE TABLE IF NOT EXISTS PAGEORDER (ID INTEGER PRIMARY KEY, PAGENUM INT)";
            if (sqlite3_exec(databaseHandle, sqlStatement, NULL, NULL, &error) == SQLITE_OK){
                sqlStatement = "CREATE TABLE IF NOT EXISTS PAGE (ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGEPATH TEXT, XCOORD INT, YCOORD INT, TITLE TEXT, TITLESET INT, EVENTDATE TEXT, EVENTDATESET INT, PANELCOLOR INT)";
                
                if (sqlite3_exec(databaseHandle, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
                {
                    // Create the ALARM table with foreign key to the ENTRY table
                    sqlStatement = "CREATE TABLE IF NOT EXISTS ALARM (ID INTEGER PRIMARY KEY AUTOINCREMENT, ALARMENABLED INT, ALARMDAYS INT, ALARMTIME TEXT, PAGEID INT, FOREIGN KEY(PAGEID) REFERENCES PAGE(ID))";
                    if (sqlite3_exec(databaseHandle, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
                    {
                        NSLog(@"Database and tables created.");
                    }
                    else
                    {
                        NSLog(@"Error: %s", error);
                    }
                }
                else
                {
                    NSLog(@"Error: %s", error);
                }
            }
            else
            {
                NSLog(@"Error: %s", error);
            }
        }
    }
}


- (void) db_addPage:(SinglePage*)page{
    // Create insert statement for the page
    NSString *queryStatement;
    char *error;
    
    // insert an entry (minus imagepath)
    queryStatement = [NSString stringWithFormat:@"INSERT INTO PAGE (XCOORD, YCOORD, TITLE, TITLESET, EVENTDATE, EVENTDATESET, PANELCOLOR) VALUES (\"%f\", \"%f\", \"%@\", \"%@\", \"%@\", \"%@\", \"%d\")",
                      page.panel.center.x, page.panel.center.y, [page getTitle], [page isTitleSet]?@"1":@"0", [self dateToStringShort:[page getDate]], [page isDateSet]?@"1":@"0", [page panelColor]];
    
    if ( sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"page (minus imagePath) inserted");
        int uid = sqlite3_last_insert_rowid(databaseHandle);
        // imagePath addition
        [page setUID:uid];
        queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET IMAGEPATH = \"%d.jpeg\" WHERE ID = %d", uid, uid];
        if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) == SQLITE_OK){
            NSLog(@"imagePath inserted");
        }
        else
            NSLog(@"Error: %s", error);
        
        // Create and insert alarm
        queryStatement = [NSString stringWithFormat:@"INSERT INTO ALARM (ALARMENABLED, ALARMDAYS, ALARMTIME, PAGEID) VALUES (\"%d\", \"%d\", \"%@\", \"%d\")",
                          [page bAlarmEnabled]?1:0, [page getAlarmDays], [self dateToStringHM:[page getAlarmTime]], uid];
        if ( sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) == SQLITE_OK){
            NSLog(@"alarm inserted");
        }
        else
            NSLog(@"Error: %s", error);
    }
    else{
        NSLog(@"Error: %s", error);
    }
}

- (void) db_deletePage:(SinglePage*)page{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"DELETE FROM ALARM WHERE PAGEID = %d", [page getUID]];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    queryStatement = [NSString stringWithFormat:@"DELETE FROM PAGE WHERE ID = %d", [page getUID]];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
}

- (void) db_updatePanelCoords:(SinglePage *)page{
    NSInteger uid = [page getUID];
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET XCOORD = \"%f\", YCOORD = \"%f\" WHERE ID = %d", page.panel.frame.origin.x, page.panel.frame.origin.y, uid];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
       NSLog(@"Error: %s", error);
}

- (void) db_updateTitle:(SinglePage*)page{
    NSInteger uid = [page getUID];
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET TITLE = \"%@\", TITLESET = %d WHERE ID = %d", [page getTitle],1, uid];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_updateDate:(SinglePage*)page{
    NSInteger uid = [page getUID];
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET EVENTDATE = \"%@\", EVENTDATESET = %d WHERE ID = %d", [self dateToStringShort:[page getDate]], 1, uid];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_updateAlarm:(SinglePage*)page{    // :(BOOL)enabled days:(NSInteger)numDaysBeforeOrAfter time:(NSDate*)time atPage:(SinglePage*)page{
    NSInteger uid = [page getUID];
    NSString *queryStatement;
    char *error;
    int daysBeforeOrAfter = [page getAlarmDays];
    
    if([page bAlarmEnabled]){
        queryStatement = [NSString stringWithFormat:@"UPDATE ALARM SET ALARMENABLED = %d, ALARMDAYS = %d, ALARMTIME = \"%@\" WHERE PAGEID = %d", [page bAlarmEnabled]?1:0, daysBeforeOrAfter, [self dateToStringHM:[page getAlarmTime]], uid];
    }
    else
        queryStatement = [NSString stringWithFormat:@"DELETE FROM ALARM WHERE PAGEID = %d", uid];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_updatePanelColor:(SinglePage*)page{
    NSInteger uid = [page getUID];
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET PANELCOLOR = \"%d\" WHERE ID = %d", [page panelColor], uid];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_clearTitle:(SinglePage*)page{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET TITLESET = 0 WHERE ID = %d", [page getUID]];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_clearDate:(SinglePage*)page{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGE SET EVENTDATESET = 0 WHERE ID = %d", [page getUID]];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_order_addPageWithUID:(NSInteger)uid atOrder:(NSInteger)order{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"INSERT INTO PAGEORDER (ID, PAGENUM) VALUES (%d, %d)", uid, order];
    NSLog(@"%@", queryStatement);
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK)
        NSLog(@"Error: %s", error);
}

- (void) db_order_removePage:(NSInteger)pageNum{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"DELETE FROM PAGEORDER WHERE PAGENUM = %d", pageNum];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }

    queryStatement = [NSString stringWithFormat:@"UPDATE PAGEORDER SET PAGENUM = PAGENUM-1 WHERE PAGENUM > %d", pageNum];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    
    
}

- (void) db_order_movePageWithUID:(NSInteger)uid from:(NSInteger)fromIndex to:(NSInteger)targetIndex{
    //update pageorder set pagenum = pagenum+1 where id>2 and id<4;
    // move uid -> targetIndex
    NSString *queryStatement;
    char *error;

    if(fromIndex < targetIndex){
        queryStatement = [NSString stringWithFormat:@"UPDATE PAGEORDER SET PAGENUM = PAGENUM-1 WHERE PAGENUM > %d AND PAGENUM <= %d", fromIndex, targetIndex];
        if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
            NSLog(@"Error: %s", error);
        }
    }
    else if(fromIndex > targetIndex){
        queryStatement = [NSString stringWithFormat:@"UPDATE PAGEORDER SET PAGENUM = PAGENUM+1 WHERE PAGENUM < %d AND PAGENUM >= %d", fromIndex, targetIndex];
        if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
            NSLog(@"Error: %s", error);
        }
    }
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGEORDER SET PAGENUM = %d WHERE ID = %d", targetIndex, uid];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    
}

- (NSInteger) db_order_getUidAtPage:(NSInteger)pageNum{
    NSString *queryStatement;
    int uid = -1;
    queryStatement = [NSString stringWithFormat:@"SELECT ID FROM PAGEORDER WHERE PAGENUM = %d", pageNum];
    NSLog(@"%@", queryStatement);
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        sqlite3_step(statement);
        uid = sqlite3_column_int(statement, 0);
        sqlite3_finalize(statement);
    }
    // Return the found address and mark for autorelease
    return uid;
}
 
- (void) db_order_updatePageNumWithUID:(NSInteger)uid atPage:(NSInteger)pageNum{
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"UPDATE PAGEORDER SET PAGENUM = %d WHERE ID = %d", pageNum, uid];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
}

#pragma mark - Reset
- (void) resetAll{
    [self deleteAllFilesInDocuments];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    NSString *queryStatement;
    char *error;
    queryStatement = [NSString stringWithFormat:@"DELETE FROM PAGE"];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    queryStatement = [NSString stringWithFormat:@"DELETE FROM PAGEORDER"];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    queryStatement = [NSString stringWithFormat:@"DELETE FROM ALARM"];
    if (sqlite3_exec(databaseHandle, [queryStatement UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error: %s", error);
    }
    
}

-(void) deleteAllFilesInDocuments{
	
	NSArray *homePaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *homeDir = [homePaths objectAtIndex:0];
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSError *error=nil;
	
	NSArray *contents = [fileManager contentsOfDirectoryAtPath:homeDir error:nil];
	
	for (NSString *file in contents) {
        if([file compare:@"sqlite.db"]==(NSComparisonResult)NSOrderedSame)
            continue;
		BOOL success = [fileManager removeItemAtPath:[homeDir stringByAppendingPathComponent:file] error:&error];
		
		if(!success){
			NSLog(@"couldn't delete file: %@",error);
        }
    }
}
      

// TABLE PAGE (ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGEPATH TEXT, XCOORD INT, YCOORD INT, TITLE TEXT, TITLESET INT, EVENTDATE TEXT, EVENTDATESET INT, PANELCOLOR INT)
// TABLE ALARM (ID INTEGER PRIMARY KEY AUTOINCREMENT, ALARMENABLED INT, ALARMDAYS INT, ALARMTIME TEXT, PAGEID INT, FOREIGN KEY(PAGEID) REFERENCES PAGE(ID))
// TABLE PAGEORDER (ID INTEGER PRIMARY KEY, PAGENUM INT)
#pragma mark - Restore


int *restorationData;
int restorationNumData;
int restorationCurrentIndex;

- (int) initRestoration{
    restorationData = malloc(MAXNUMENTRIES * sizeof(int));
    restorationCurrentIndex = 0;
    
    NSString *queryStatement;
    sqlite3_stmt *statement;
    int numEntries = 0;
    int id_;
    int pagenum_;
    
    queryStatement = @"SELECT ID, PAGENUM FROM PAGEORDER";
    if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(statement) == SQLITE_ROW){
            numEntries++; 
            id_ = sqlite3_column_int(statement, 0);
            pagenum_ = sqlite3_column_int(statement, 1);
            restorationData[pagenum_] = id_;
        }
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Error sqlite3_prepare at %s   -  %d", __func__, __LINE__);
    }
    restorationNumData = numEntries;
    return numEntries;
}

- (SinglePage*) getNext{
    if(restorationCurrentIndex >= restorationNumData)
        return NULL;
    
    SinglePage *p = [[SinglePage alloc] initWithFrame:CGRectMake(restorationCurrentIndex*screenWidth, 0, screenWidth, screenHeight)];
    sqlite3_stmt *statement;
    int uid = restorationData[restorationCurrentIndex];

    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM PAGE WHERE ID = %d", uid];
    if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        NSLog(@"%@", queryStatement);
        sqlite3_step(statement);
        [p setUID:uid];
        NSLog(@"%d    x:%d    y:%d   eventDate:%@    titleSet:%d     panelColor:%d", uid, sqlite3_column_int(statement, 2), sqlite3_column_int(statement, 3), [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)],  sqlite3_column_int(statement, 7), sqlite3_column_int(statement, 8)   );
        
        [p setBackgroundImage:[UIImage imageWithData:[self readFromFile:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)]]] save:NO];
        [p.panel setFrame:CGRectMake(sqlite3_column_int(statement, 2), sqlite3_column_int(statement, 3), p.panel.frame.size.width, p.panel.frame.size.height)];
        [p setTitle:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)] saveToDB:false];
        [p setBIsTitleSet:(sqlite3_column_int(statement, 5)==1)?true:false];
        [p setDate:[self string_short_ToDate:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)]] saveToDB:false];
        [p setBIsTitleSet:(sqlite3_column_int(statement, 7)==1)?true:false];
        [p setPanelColor:(sqlite3_column_int(statement, 8))];
        sqlite3_finalize(statement);
    }
    
    queryStatement = [NSString stringWithFormat:@"SELECT * FROM ALARM WHERE PAGEID = %d", uid];
    if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        NSLog(@"%@", queryStatement);
        sqlite3_step(statement);
        NSLog(@"%d    alarmEnabled=%d    alarmDays=%d     alarmTime=%s", uid, sqlite3_column_int(statement, 1), sqlite3_column_int(statement, 2), (char*)sqlite3_column_text(statement, 3));
        
        [p setBAlarmEnabled:(sqlite3_column_int(statement, 1)==1)?true:false];
        if([p bAlarmEnabled]){
            [p setAlarmDays:sqlite3_column_int(statement, 2)];
            [p setAlarmTime:[self string_hm_ToDate:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)]]];
        }
        
        sqlite3_finalize(statement);
    }
    NSLog(@"============");
    [entries addObject:p];
    NSLog(@"num entries = %d", [entries count]);

    restorationCurrentIndex++;
    return p;
}


- (void) restoreAtInit{
    NSString *queryStatement;
    int numEntries = 0;
    int uid = -1;
    
    queryStatement = @"SELECT COUNT(*) FROM PAGEORDER";
    NSLog(@"%@", queryStatement);
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        sqlite3_step(statement);
        numEntries = sqlite3_column_int(statement, 0);
        sqlite3_finalize(statement);
    }
  
    for(int i=0; i<numEntries; i++){
        SinglePage *p = [[SinglePage alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        queryStatement = [NSString stringWithFormat:@"SELECT ID FROM PAGEORDER WHERE PAGENUM = %d", i];
        NSLog(@"%@", queryStatement);

        if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            sqlite3_step(statement);
            uid = sqlite3_column_int(statement, 0);
            sqlite3_finalize(statement);
        }
        queryStatement = [NSString stringWithFormat:@"SELECT * FROM PAGE WHERE ID = %d", uid];
        if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"%@", queryStatement);
            sqlite3_step(statement);
            [p setUID:uid];
            NSLog(@"%d    x:%d    y:%d   eventDate:%@    titleSet:%d", uid, sqlite3_column_int(statement, 2), sqlite3_column_int(statement, 3), [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)],  sqlite3_column_int(statement, 7)   );
            
            [p setBackgroundImage:[UIImage imageWithData:[self readFromFile:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)]]] save:YES];
            [p.panel setFrame:CGRectMake(sqlite3_column_int(statement, 2), sqlite3_column_int(statement, 3), p.panel.frame.size.width, p.panel.frame.size.height)];
            [p setTitle:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)] saveToDB:false];
            [p setBIsTitleSet:(sqlite3_column_int(statement, 5)==1)?true:false];
            [p setDate:[self string_short_ToDate:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)]] saveToDB:false];
            [p setBIsTitleSet:(sqlite3_column_int(statement, 7)==1)?true:false];
            sqlite3_finalize(statement);
        }
        
        queryStatement = [NSString stringWithFormat:@"SELECT * FROM ALARM WHERE PAGEID = %d", uid];
        if (sqlite3_prepare_v2(databaseHandle, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            NSLog(@"%@", queryStatement);
            sqlite3_step(statement);
            NSLog(@"%d    alarmEnabled=%d    alarmDays=%d     alarmTime=%s", uid, sqlite3_column_int(statement, 1), sqlite3_column_int(statement, 2), (char*)sqlite3_column_text(statement, 3));

            [p setBAlarmEnabled:(sqlite3_column_int(statement, 1)==1)?true:false];
            [p setAlarmDays:sqlite3_column_int(statement, 2)];
            [p setAlarmTime:[self string_hm_ToDate:[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)]]];
            sqlite3_finalize(statement);
        }
        NSLog(@"============");
       [entries addObject:p];
        NSLog(@"num entries = %d", [entries count]);
    }

}





@end

