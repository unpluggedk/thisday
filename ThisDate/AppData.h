//
//  AppData.h
//  ThisDate
//
//  Created by jason kim on 8/16/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SinglePage/SinglePage.h"
#import "SinglePage/SinglePageIndexData.h"
#import <sqlite3.h>
#import "SettingsPage/SettingsViewController.h"

#import <DropboxSDK/DropboxSDK.h>


#ifdef LITE_VERSION
#define MAXNUMENTRIES 2

#else
#define MAXNUMENTRIES 30
#endif

@protocol AppDataDelegate
- (void) updatePanelProgress:(float)percentage;
- (void) updateCompleted:(bool)success;
@end


@interface AppData : NSObject <DBRestClientDelegate>{
    NSMutableArray *entries;
    NSArray *sortedIndices;

    DBRestClient *restClient;

    int screenWidth;
    int screenHeight;
   
}


@property int screenWidth;
@property int screenHeight;

@property (nonatomic, retain) NSMutableArray *entries;
@property (nonatomic, retain) NSArray *sortedIndices;

@property (nonatomic, weak) id<AppDataDelegate> delegate;


+ (id)sharedAppData;
// count from one
- (void)setCountFromOne:(bool)newVal;
- (bool)getCountFromOne;

// dropbox functions
- (void)linkDropBoxWith:(UIViewController*)viewController;
- (void)unlinkDropbox;
- (DBRestClient *)restClient;
- (void)linkCompleted:(bool)result;
- (bool)isDropboxLinked;

// Page commands
- (void)removeEntryAtPage:(int)pageNum;
- (bool)pre_addEntry:(SinglePage*)entry atPage:(int)pageNum;
- (bool)addEntry:(SinglePage*)entry atPage:(int)pageNum;
- (void)moveEntryAtPage:(int)fromPage toPage:(int)destinationPage;
- (int)getNumEntries;
- (SinglePage*)getPageAt:(int)targetPage;
- (SinglePage*)getPageOfUID:(int)uid;
- (int)getPageOrderOfUID:(int)uid;
- (void)togglePanelAlphaAtPage:(int)pageNum;

// Date, String conversion
- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

- (NSString*)dateToString:(NSDate*)date;
- (NSString*)dateToStringShort:(NSDate*)date;
- (NSDate*)dateWithZeroHMS:(NSDate*)date;
- (NSDate*)stringToDate:(NSString*)str;
- (NSString*)dateToStringHM:(NSDate*)date;

- (NSDate*)string_short_ToDate:(NSString*)str;
- (NSDate*)string_hm_ToDate:(NSString*)str;

// Sorting
- (void)sort;
- (int)getOrderAtPage:(int)page;
- (void)sortCompleted;

// File IO
-(BOOL)writeToFile:(UIImage *)image fileName:(NSString *)fileName;
-(NSData *)readFromFile:(NSString *)fileName;
-(BOOL)deleteFile:(NSString*) fileName;

// Restoration & reset
- (int) initRestoration;
- (SinglePage*) getNext;
- (void) restoreAtInit;
- (void) resetAll;

// DB
- (void) db_addPage:(SinglePage*)page;
- (void) db_deletePage:(SinglePage*)page;
- (void) db_updateTitle:(SinglePage*)page;
- (void) db_updatePanelCoords:(SinglePage*)page;
- (void) db_updateDate:(SinglePage*)page;
- (void) db_updateAlarm:(SinglePage*)page;
- (void) db_updatePanelColor:(SinglePage*)page;
- (void) db_clearTitle:(SinglePage*)page;
- (void) db_clearDate:(SinglePage*)page;
- (void) db_order_addPageWithUID:(NSInteger)uid atOrder:(NSInteger)order;
- (void) db_order_removePage:(NSInteger)pageNum;
- (void) db_order_movePageWithUID:(NSInteger)uid from:(NSInteger)fromIndex to:(NSInteger)targetIndex;
- (NSInteger) db_order_getUidAtPage:(NSInteger)pageNum;
- (void) db_order_updatePageNumWithUID:(NSInteger)uid atPage:(NSInteger)pageNum;

// Notifications
- (void) scheduleNotificationOn:(NSDate*) fireDate
						   text:(NSString*) alertText
						 action:(NSString*) alertAction
						  sound:(NSString*) soundfileName
					launchImage:(NSString*) launchImage
						andInfo:(NSDictionary*) userInfo;


- (void) handleReceivedNotification:(UILocalNotification*) thisNotification;

- (void) deleteNotification:(SinglePage *)page;
- (void) updateNotification:(SinglePage *)page;

@end
