//
//  TopPageViewController.h
//  ThisDate
//
//  Created by jason kim on 8/1/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppScrollView.h"
#import "ScrollDelegate.h"
#import "AppData.h"
#import "InfoPage/InfoPageViewController.h"
#import "SettingsPage/SettingsViewController.h"
#import "ProgressViewWithLabel/ProgressViewWithLabel.h"
#import <DropboxSDK/DropboxSDK.h>


#define TOTAL_PAGES 1000
#define ACTIONSHEET_ADDPAGE 2000
#define ACTIONSHEET_SHARE 2001
#define ACTIONSHEET_DELETE 2002

#define LISTVIEW_THUMBNAIL 3000
#define LISTVIEW_TOPLEFT_LABEL 3001
#define LISTVIEW_TOPRIGHT_LABEL 3002
#define LISTVIEW_TITLE_LABEL 3003

#define TOUCH_IGNORE 9999

@interface TopPageViewController : UIViewController<UIGestureRecognizerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, InfoPageViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, SettingsViewControllerDelegate, CustomScrollViewDelegate, AppDataDelegate>
{
    AppScrollView *contentScrollView;
    NSInteger numPages;
    UIPageControl *pageControl;
    ScrollDelegate *scrollDelegate;
    
    bool bToolbarShowing;
    bool bListviewShowing;
    
    CGFloat firstX;
    CGFloat firstY;
    
    int actionSheetType;
    UIImage *addPageImage;
    
    AppData *appData;
    
    UIBarButtonItem *toolbar_shareButton;
    UIBarButtonItem *toolbar_listButton;
    UIBarButtonItem *toolbar_deleteButton;
    UIBarButtonItem *toolbar_addButton;
    UIBarButtonItem *toolbar_infoButton;
    UIBarButtonItem *toolbar_setupButton;
    UIBarButtonItem *toolbar_helpButton;
    
    NSMutableArray *toolbarItems;
   
    ProgressViewWithLabel *progressView;
    NSArray *progressBarItems;
    
    
    NSUserDefaults *pref;
    
    NSCache *imageCache;
    
    UIImageView *helpPage;
    bool bHelpShown;
    
    UIButton *helpButton;

}


@property (nonatomic, retain) AppScrollView *contentScrollView;


- (NSInteger) getCurrentPage;

- (id)initWithNotification:(UILocalNotification*)notification;
- (void)notificationHandler:(UILocalNotification*)notification;
@end