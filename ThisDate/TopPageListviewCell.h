//
//  TopPageListviewCell.h
//  ThisDate
//
//  Created by jason kim on 9/15/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel/OHAttributedLabel.h"

@interface TopPageListviewCell : UITableViewCell{
    UIImageView *image_;
    OHAttributedLabel *topLeftLabel;
    UILabel *topRightLabel;
    UILabel *titleLabel;
}

@property(nonatomic, retain) UIImageView *image_;
@property(nonatomic, retain) OHAttributedLabel *topLeftLabel;
@property(nonatomic, retain) UILabel *topRightLabel;
@property(nonatomic, retain) UILabel *titleLabel;
    
@end
