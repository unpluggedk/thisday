//
//  TopPageViewController.m
//  DayTracker
//
//  Created by jason kim on 7/31/12.
//  Copyright (c) 2012 yogurtfire. All rights reserved.
//

#import "TopPageViewController.h"
#import "SinglePage/SinglePage.h"
#import <QuartzCore/QuartzCore.h>
#import "OHAttributedLabel/NSAttributedString+Attributes.h"
#import "OHAttributedLabel/OHAttributedLabel.h"
#import "TopPageListviewCell.h"
#import "SettingsViewController.h"
#import "UIImage+Resize.h"


@interface TopPageViewController ()

@end

@implementation TopPageViewController

@synthesize contentScrollView;

#define HELPPAGE 101

#pragma mark - Notification handler

NSInteger targetPage;


- (void)notificationHandler:(UILocalNotification*)notification{
    if(notification == nil){
        return;
    }
    
//    UIAlertView *alert;
    NSInteger uid = ([[notification.userInfo valueForKey:@"uid"] intValue]);
    targetPage = [appData getPageOrderOfUID:uid];

    if(targetPage != -1){
        [contentScrollView scrollRectToVisible:CGRectMake([appData screenWidth]*targetPage, 0, [appData screenWidth], [appData screenHeight]) animated:YES];

//        alert = [[UIAlertView alloc] initWithTitle:@"Following Event has occured"
  //                                         message:[[appData getPageOfUID:uid] getTitle] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"View", nil];
    }
    else{
        ;
    }
    [appData handleReceivedNotification:notification];

    NSLog(@"targetPageindex = %d", targetPage);
/*    if(alert)
        [alert show];
*/
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        ;
    }
    else{
        [self dismissListView];
        [self dismissViewControllerAnimated:YES completion:nil];
        [contentScrollView scrollRectToVisible:CGRectMake([appData screenWidth]*targetPage, 0, [appData screenWidth], [appData screenHeight]) animated:YES];
    }
    
}




#pragma mark - Initialization
UILocalNotification* startupNotification;
- (id)initWithNotification:(UILocalNotification*)notification{
    NSLog(@"INIT");
    self = [super init];
    if (self) {
        
        // Custom initialization
        appData = [AppData sharedAppData];
        
        [appData setScreenWidth:[[UIScreen mainScreen] bounds].size.width];
        [appData setScreenHeight:[[UIScreen mainScreen] bounds].size.height];
        
        pref = [NSUserDefaults standardUserDefaults];
        
        imageCache = [[NSCache alloc] init];
        [imageCache setName:@"ImageCache"];
        
        helpPage = NULL;
        bHelpShown = false;
        
     
        //  appData->item.bgImagePath = "222";
        //  NSLog([NSString stringWithUTF8String:appData->item.bgImagePath]);
    }
    
    startupNotification = notification;
    //[self.view setAutoresizesSubviews:NO];
    return self;

}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"INIT WITH NIB");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        // Custom initialization
        appData = [AppData sharedAppData];
        
        [appData setScreenWidth:[[UIScreen mainScreen] bounds].size.width];
        [appData setScreenHeight:[[UIScreen mainScreen] bounds].size.height];
        
        pref = [NSUserDefaults standardUserDefaults];
        
        imageCache = [[NSCache alloc] init];
        [imageCache setName:@"ImageCache"];
        
        helpPage = NULL;
        bHelpShown = false;
        
        restoreLock = [[NSCondition alloc] init];
      //  appData->item.bgImagePath = "222";
      //  NSLog([NSString stringWithUTF8String:appData->item.bgImagePath]);
    }
    //[self.view setAutoresizesSubviews:NO];
    return self;
}
*/


- (void)didReceiveMemoryWarning{
    [imageCache removeAllObjects];
    [super didReceiveMemoryWarning];
}

- (UIBarButtonItem*)barButtonSystemItemFlexibleSpace{
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
}

- (NSInteger) getCurrentPage{
    return pageControl.currentPage;
}

- (void)disable_toolbarIcons{
    [toolbar_shareButton setEnabled:NO];
    [toolbar_listButton setEnabled:NO];
    [toolbar_deleteButton setEnabled:NO];
    [toolbar_infoButton setEnabled:NO];
}

UIActivityIndicatorView *indicator;
bool bIgnoreInput;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[appData scheduleNotificationOn:[NSDate dateWithTimeIntervalSinceNow:20]
    //                           text:@"Hey there" action:@"View" sound:nil launchImage:nil andInfo:nil];
    
    // load!!!
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    bIgnoreInput = true;
    indicator.frame = CGRectMake(0,0,50,50);
    indicator.center = self.view.center;
    indicator.hidesWhenStopped = true;
    [self.view addSubview:indicator];
    
    [indicator startAnimating];
    
    
    // Init variables
    numPages = 0;
    bToolbarShowing = false;
    bListviewShowing = false;
    
    
    // ==================================================
    // set up toolbar 
    // ==================================================
    
     toolbar_shareButton = [[UIBarButtonItem alloc]
                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                              target:self
                              action:@selector(shareActionSheet)];
    [toolbar_shareButton setEnabled:NO];
    [toolbar_shareButton setStyle:UIBarButtonItemStylePlain];
    
    toolbar_helpButton = [[UIBarButtonItem alloc]
                          initWithImage:[UIImage imageNamed:@"help.png"]
                          style:UIBarButtonItemStylePlain target:self action:@selector(toggleHelp)];
    
    toolbar_listButton = [[UIBarButtonItem alloc]
                          initWithImage:[UIImage imageNamed:@"icon_list_bullets.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showList)];
    [toolbar_listButton setEnabled:NO];
    toolbar_listButton.imageInsets = UIEdgeInsetsMake(2.0f, 0, -2.0, 0);
    
    toolbar_deleteButton = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                target:self
                                action:@selector(trashActionSheet)];
    [toolbar_deleteButton setEnabled:NO];
    [toolbar_deleteButton setStyle:UIBarButtonItemStylePlain];
    
    toolbar_addButton = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                target:self
                                action:@selector(addPageActionSheet)];
    [toolbar_addButton setStyle:UIBarButtonItemStylePlain];
    
    toolbar_infoButton = [[UIBarButtonItem alloc]
                                     initWithImage:[UIImage imageNamed:@"doc_info.png"]
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(openInfoPage:)];

    [toolbar_infoButton setEnabled:NO];
    toolbar_infoButton.imageInsets = UIEdgeInsetsMake(3.0f, 0, -3.0, 0);
    
    toolbar_setupButton = [[UIBarButtonItem alloc]
                           initWithImage:[UIImage imageNamed:@"gear.png"]
                           style:UIBarButtonItemStylePlain
                           target:self
                           action:@selector(openSettingsPage:)];
    [toolbar_setupButton setEnabled:YES];
    toolbar_setupButton.imageInsets = UIEdgeInsetsMake(3.0f, 0, -3.0, 0);
    
    toolbarItems = [NSMutableArray arrayWithObjects:
//    NSArray *buttons = [NSArray arrayWithObjects:
                        toolbar_shareButton, [self barButtonSystemItemFlexibleSpace],
                        toolbar_listButton, [self barButtonSystemItemFlexibleSpace],
                        toolbar_addButton, [self barButtonSystemItemFlexibleSpace],
                        toolbar_infoButton, [self barButtonSystemItemFlexibleSpace],
                        toolbar_setupButton,[self barButtonSystemItemFlexibleSpace],
                        toolbar_deleteButton,
                        nil];
    self.toolbarItems = toolbarItems;
    self.navigationController.toolbar.barStyle = UIBarStyleBlackTranslucent;
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
 
    // ==================================================
    // set up progressbar
    // ==================================================
    progressView = [[ProgressViewWithLabel alloc] init];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:progressView];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    progressBarItems = [NSArray arrayWithObjects:flexibleSpace, barButton, flexibleSpace, nil];
    
    
    // ==================================================
    // set up main screen
    // ==================================================
    contentScrollView = [[AppScrollView alloc] initWithFrame:CGRectMake(0, 0, [appData screenWidth], [appData screenHeight])];
    [contentScrollView setContentSize:CGSizeMake(0, 0)];
    [contentScrollView setShowsVerticalScrollIndicator:NO];
    //[contentScrollView setCanCancelContentTouches:YES];
    //contentScrollView = NO;
//    [contentScrollView setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [contentScrollView setBackgroundColor:[UIColor viewFlipsideBackgroundColor]];
    [contentScrollView setPagingEnabled:YES];
    [contentScrollView setAlpha:1.0];
    [contentScrollView setAlwaysBounceHorizontal:NO];
    
    // add page control
    pageControl = [[UIPageControl alloc] init];
    pageControl.userInteractionEnabled = NO;
    pageControl.frame = CGRectMake(0, [appData screenHeight] - 30, [appData screenWidth], 30);
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.numberOfPages = numPages;
    pageControl.currentPage = 0;
    pageControl.hidesForSinglePage = NO;
    
    scrollDelegate = [[ScrollDelegate alloc] initWithPageController:pageControl];
    scrollDelegate.delegate = self;
    [scrollDelegate setSinglePageHeight:[appData screenHeight]];
    [scrollDelegate setSinglePageWidth:[appData screenWidth]];
    contentScrollView.delegate = scrollDelegate;
    
    [self.view addSubview:contentScrollView];
    [self.view insertSubview:contentScrollView belowSubview:indicator];
    [self.view addSubview:pageControl];
    
    listView = [self createListView];
    [self.view addSubview:listView];
   
    [self performSelectorInBackground:@selector(restoreStart) withObject:nil];

}


int restorePage;
- (void)restoreStart{
    restorePage = [pref integerForKey:@"lastpage"];
    int numPage = [appData initRestoration];
    [contentScrollView setContentSize:CGSizeMake(numPage * [appData screenWidth], [appData screenHeight])];
    [contentScrollView scrollRectToVisible:CGRectMake([appData screenWidth]*[pref integerForKey:@"lastpage"], 0, [appData screenWidth], [appData screenHeight]) animated:NO];
    bool loop = true;
    
    while(loop){
        SinglePage *p = [appData getNext];
        
        if(p == NULL){
            loop = false;
            continue;
        }
        [self performSelectorOnMainThread:@selector(restorationAddPage:) withObject:p waitUntilDone:NO];
    }
    NSLog(@"restoreStart()  done!");
    [self performSelectorOnMainThread:@selector(finishRestoration) withObject:NULL waitUntilDone:NO];
}

- (void)restorationAddPage:(SinglePage*)p{
    NSLog(@"restorationAddPage start");
    [p setUserInteractionEnabled:YES];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapped:)];
    [tapRecognizer setNumberOfTapsRequired:2];
    [tapRecognizer setDelegate:self];
    
    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(shortPressed:)];
    [singleTapRecognizer setNumberOfTapsRequired:1];
    [singleTapRecognizer setDelegate:self];
    
    UILongPressGestureRecognizer *longPressRecognizer =[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [longPressRecognizer setMinimumPressDuration:0.1];
    [longPressRecognizer setDelegate:self];
    
    [[p panel] addGestureRecognizer:tapRecognizer];
    [[p panel] addGestureRecognizer:singleTapRecognizer];
    [[p panel] addGestureRecognizer:longPressRecognizer];
  
    [p refreshPanel];

    /*
    if(numPages == restorePage){
        [p setBInView:true];
        [p updateQuality:kCGInterpolationHigh];
        
    }
     */
        
    pageControl.numberOfPages++;
    numPages++;
    

    [contentScrollView addSubview:p];
    NSLog(@"restorationAddPage end");
    
}

- (void)finishRestoration{
    NSLog(@"finishRestoration");
    if(numPages > 0){
        [toolbar_shareButton setEnabled:YES];
        [toolbar_listButton setEnabled:YES];
        [toolbar_deleteButton setEnabled:YES];
        [toolbar_infoButton setEnabled:YES];
        
        if(numPages == MAXNUMENTRIES){
            [toolbar_addButton setEnabled:NO];
        }
    }
    else{
        [self showHelp];
        [self setToolbarHidden:NO];
    }

    pageControl.currentPage= [pref integerForKey:@"lastpage"];
    
    //[[appData getPageAt:pageControl.currentPage] showBg:true];
    //if(pageControl.currentPage>0)
    //    [[appData getPageAt:pageControl.currentPage-1] showBg:true];
    //if(pageControl.currentPage<pageControl.numberOfPages-1)
    //    [[appData getPageAt:pageControl.currentPage+1] showBg:true];
    
    if([[appData entries] count] == 0){
        [toolbarItems replaceObjectAtIndex:0 withObject:toolbar_helpButton];
    }
    
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    NSLog(@"current page = %d", pageControl.currentPage);
    [p setBInView:true];
    [p updateQuality:kCGInterpolationHigh];
    
    [indicator stopAnimating];
    [self notificationHandler:startupNotification];
    
    bIgnoreInput = false;
}
/*
 NSArray *buttons = [NSArray arrayWithObjects:
 toolbar_shareButton, [self barButtonSystemItemFlexibleSpace],
 toolbar_listButton, [self barButtonSystemItemFlexibleSpace],
 toolbar_addButton, [self barButtonSystemItemFlexibleSpace],
 toolbar_infoButton, [self barButtonSystemItemFlexibleSpace],
 toolbar_setupButton,[self barButtonSystemItemFlexibleSpace],
 toolbar_deleteButton,
 nil];
 self.toolbarItems = buttons;
*/
/*
- (void)restorationDone{
    // load at startup
    int numEntries = [appData getNumEntries];
    NSLog(@"restoration %d", numEntries);
    [contentScrollView setContentSize:CGSizeMake(320*numEntries, 480)];
    for(int i=0; i<[[appData entries] count]; i++){
        SinglePage *p = [appData getPageAt:i];
        [p setFrame:CGRectMake(320*i, 0, 320, 480)];
        [p setUserInteractionEnabled:YES];
        
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(move:)];
        [panRecognizer setMinimumNumberOfTouches:1];
        [panRecognizer setMaximumNumberOfTouches:1];
        [panRecognizer setDelegate:self];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(tapped:)];
        [tapRecognizer setNumberOfTapsRequired:2];
        [tapRecognizer setDelegate:self];
        
        UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(shortPressed:)];
        [singleTapRecognizer setNumberOfTapsRequired:1];
        [singleTapRecognizer setDelegate:self];
        
        UILongPressGestureRecognizer *longPressRecognizer =[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
        [longPressRecognizer setMinimumPressDuration:0.1];
        [longPressRecognizer setDelegate:self];
        
        [[p panel] addGestureRecognizer:tapRecognizer];
        [[p panel] addGestureRecognizer:singleTapRecognizer];
        [[p panel] addGestureRecognizer:longPressRecognizer];
        
        [[p panel] reloadInputViews];
        
        pageControl.numberOfPages++;
        numPages++;
        
        [contentScrollView addSubview:p];
       // [[[p multiDialController] dial4] spinToString:@"0"];
    }
    
    if(numPages > 0){
        [toolbar_shareButton setEnabled:YES];
        [toolbar_listButton setEnabled:YES];
        [toolbar_deleteButton setEnabled:YES];
        [toolbar_infoButton setEnabled:YES];
//        [contentScrollView scrollRectToVisible:CGRectMake(320*0, 0, 320, 480) animated:NO];
        
        [contentScrollView scrollRectToVisible:CGRectMake(320*[pref integerForKey:@"lastpage"], 0, 320, 480) animated:NO];
        
    }
    bRestoring = false;
    [indicator stopAnimating];
}

- (void)restore{
    [appData restoreAtInit];
    [self restorationDone];
    //[self performSelectorOnMainThread:@selector(restorationDone) withObject:nil waitUntilDone:YES];
}
*/
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Help
- (void) toggleHelp{
    if(bHelpShown)
        [self hideHelp];
    else
        [self showHelp];
    
}

- (void) showHelp{
    if(!helpPage){
        if([appData screenHeight] == 1136)
            helpPage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"helppage_iphone5.png"]];
        else
            helpPage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"helppage_iphone4.png"]];
        [helpPage setFrame:CGRectMake(0, 0, [appData screenWidth], [appData screenHeight])];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [self.view addSubview:helpPage];
    bHelpShown = true;
}

- (void) hideHelp{
    bIgnoreInput = true;
    if(helpPage){
        [UIView transitionWithView:self.view
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^ { [helpPage removeFromSuperview]; }
                        completion:^(BOOL finished){
                            bIgnoreInput = false;
                            bToolbarShowing = true;
                            [self setToolbarHidden:NO];
                        }];
    }

    bHelpShown = false;
}


#pragma mark - Touches

- (void)setToolbarHidden:(bool)hidden{
    bToolbarShowing = !hidden;
    [[UIApplication sharedApplication] setStatusBarHidden:hidden?YES:NO withAnimation:UIStatusBarAnimationFade];
    [self.navigationController setToolbarHidden:hidden?YES:NO animated:YES];
    //[helpButton setAlpha:hidden?0.0:1.0];
}



- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if(bIgnoreInput)
        return;
    
    if(bHelpShown){
        [self hideHelp];
        return;
    }
        
    
    UITouch *touch = [touches anyObject];
    if ([[touch view] tag] == TOUCH_IGNORE){
        return;
    }
    if(bListviewShowing){
        [self dismissListView];
        return;
    }
    
    if(!bToolbarShowing){
        bToolbarShowing = true;
        [self setToolbarHidden:false];
    }
    else{
        bToolbarShowing = false;
        [self setToolbarHidden:true];

    }
}


#pragma mark - Gestures
// =====================================================
// gesture handling
// =====================================================

-(void)move:(id)sender {
    NSLog(@"MOVEMOVEMOVE");
	//NSLog(@"See a move gesture");
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    
    [[sender view] setCenter:translatedPoint];
}

-(void)tapped:(id)sender {
	NSLog(@"See a tap gesture");
    [appData togglePanelAlphaAtPage:pageControl.currentPage];
    /*
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    if([p panelColor] == 1){
        NSLog(@"1");
        [p setPanelColor:0];
        [appData db_updatePanelColor:p];
//        p.panelColorReverse = false;
//        [p.panel setAlpha:0.7];
    }
    else{
        NSLog(@"2");
        [p setPanelColor:1];
        //p.panelColorReverse = true;
        //[p.panel setAlpha:1.0];
    }
     */
    /*
     if ([((SinglePage*)[[sender view] superview]) isPanelSizeBig]){
     [((SinglePage*)[[sender view] superview]) panelSizeSmall];
     }
     else
     [((SinglePage*)[[sender view] superview]) panelSizeBig];
     */
}

-(void)shortPressed:(UILongPressGestureRecognizer*)sender {
    //NSLog(@"short pressed");
}

CGPoint _priorPoint;
-(void)longPressed:(UILongPressGestureRecognizer*)sender{
    //NSLog(@"long pressed");

    int reverse = [[appData getPageAt:pageControl.currentPage] panelColor];
    
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        
        [[sender view] setAlpha:reverse?0.7:1.0];
        _priorPoint = [sender locationInView:[sender view].superview];
        //older[((SinglePage*)[sender view]) setBackgroundColor:[UIColor blueColor]];

    }
    else if(sender.state == UIGestureRecognizerStateChanged)
    {
        CGPoint center;
        //move your views here.
        UIView *view = sender.view;
        CGPoint point = [sender locationInView:view.superview];
        center = view.center;
        center.x += point.x - _priorPoint.x;
        center.y += point.y - _priorPoint.y;
        view.center = center;
        
        _priorPoint = point;
    }
    else if(sender.state == UIGestureRecognizerStateEnded)
    {
        [appData db_updatePanelCoords:[appData getPageAt:pageControl.currentPage]];
        [[sender view] setAlpha:reverse?1.0:0.7];
//        [[sender view] setAlpha:0.7];
//newer        [[sender view] setBackgroundColor:[[appData getPageAt:pageControl.currentPage] panelUnselectedColor]];
//older        [((SinglePage*)[sender view]) setBackgroundColor:[UIColor whiteColor]];
    }
    
}

#pragma mark - Action sheet
// =====================================================
// action sheet makeup
// =====================================================
- (void)setButtonOnActionSheet:(UIActionSheet*)sheet atIndex:(NSInteger)buttonIndex toState:(BOOL)enabled{
    for (UIView* view in sheet.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            if (buttonIndex == 0) {
                if ([view respondsToSelector:@selector(setEnabled:)])
                {
                    UIButton* button = (UIButton*)view;
                    button.enabled = enabled;
                }
            }
            buttonIndex--;
        }
    }
}




- (void)addPageActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Choose a background"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Camera"];
    [sheet addButtonWithTitle:@"Photos"];
    [sheet addButtonWithTitle:@"Random"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 3;
    [sheet showInView:self.view];
    
    actionSheetType = ACTIONSHEET_ADDPAGE;
}


- (void)trashActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Are you sure to delete?"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Delete"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.destructiveButtonIndex = 0;
    sheet.cancelButtonIndex = 1;
    [sheet showInView:self.view];
    
    actionSheetType = ACTIONSHEET_DELETE;
}

- (void)shareActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
//    [sheet setTitle:@"Share"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Save to Photos"];
    [sheet addButtonWithTitle:@"Save to Dropbox"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 2;
    
    if(![appData isDropboxLinked])
        [self setButtonOnActionSheet:sheet atIndex:1 toState:false];
    /*
    int buttonIndex = 1;
    for (UIView* view in sheet.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            if (buttonIndex == 0) {
                if ([view respondsToSelector:@selector(setEnabled:)])
                {
                    UIButton* button = (UIButton*)view;
                    button.enabled = false;
                }
            }
            buttonIndex--;
        }
    }
    */
    [sheet showInView:self.view];
    
    
    actionSheetType = ACTIONSHEET_SHARE;
}


int lastrandomImage = -1;

UIImagePickerController *picker;
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheetType == ACTIONSHEET_ADDPAGE){
        if(bHelpShown){
            [self hideHelp];
            [self.navigationController setToolbarHidden:YES animated:NO];
            [toolbarItems replaceObjectAtIndex:0 withObject:toolbar_shareButton];
            [self.navigationController setToolbarHidden:NO animated:NO];
        }
        
        if(buttonIndex == actionSheet.cancelButtonIndex)
            return;
        else if(buttonIndex == 0){
            // camera
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
            if([UIImagePickerController isSourceTypeAvailable:sourceType]){
                picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.sourceType = sourceType;
                [self presentModalViewController:picker animated:YES];
                
            }
            
        }
        else if(buttonIndex == 1){
            // photo
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            if([UIImagePickerController isSourceTypeAvailable:sourceType]){
                picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.sourceType = sourceType;
                [self presentModalViewController:picker animated:YES];


            }
        }
        else{
            // random
            if(lastrandomImage == -1)
                lastrandomImage = 1 + arc4random_uniform(2);
            else
                lastrandomImage = (lastrandomImage+1)%3 + 1;
            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"d%d.png", lastrandomImage]];
            [self addPageWithImage:img];
    
        }
    }
    else if(actionSheetType == ACTIONSHEET_DELETE){
        if(buttonIndex == 0){
            [self deleteEntryAtPage:pageControl.currentPage currentPage:pageControl.currentPage hideAtEnd:false];
            [self dismissListView];

        }
    }
    else if(actionSheetType == ACTIONSHEET_SHARE){
        if(buttonIndex == 0){ //save to photos
            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
                //UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
            else
                UIGraphicsBeginImageContext(self.view.bounds.size);
            [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            
        }
        else if(buttonIndex == 1){ // save to dropbox
            if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
                UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
            else
                UIGraphicsBeginImageContext(self.view.bounds.size);
            [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [appData writeToFile:image fileName:@"foo.jpeg"];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *localPath = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0], @"foo.jpeg"];
            NSString *destDir = @"/SavedImages";
            [[appData restClient] uploadFile:[NSString stringWithFormat:@"%@.jpeg", [[appData getPageAt:pageControl.currentPage] getTitle]]
                                      toPath:destDir withParentRev:nil fromPath:localPath];
            [[appData restClient] setDelegate:appData];

            [progressView.progressView setProgress:0.0 animated:NO];
            self.toolbarItems = progressBarItems;

            progressView.textLabel.text = @"Uploading in progress";
            
            appData.delegate = self;
            //
            // add delegate methods
            
            
        }
        
    }

}


#pragma mark - appData delegate on DB upload
- (void) updatePanelProgress:(float)percentage{
    NSLog(@"input %f", percentage);
    [progressView.progressView setProgress:percentage animated:YES];

}
- (void) updateCompleted:(bool)success{
    progressView.textLabel.text = success?@"Completed!":@"Please try again";
    [self performSelector:@selector(hideProgressBar) withObject:self afterDelay:1.0];
}

-(void)hideProgressBar{
    self.toolbarItems = toolbarItems;
    progressView.textLabel.text = @"";

}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    [self dismissViewControllerAnimated:YES completion:^{
        ;//       [self addPageWithImage:image];
    }];
     [NSThread detachNewThreadSelector:@selector(addPageWithImage:) toTarget:self withObject:image];

}

#pragma mark - Add page
// ===================================================
//  add page
// ===================================================
- (void)addPageWithImage:(UIImage*)image
{
    numPages++;
    
    if(numPages == MAXNUMENTRIES)
        [toolbar_addButton setEnabled:NO];
    
    pageControl.numberOfPages++;
    //pageControl.currentPage++;
    [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*(numPages), [appData screenHeight])];
    
    // scroll to added page
    //CGRect frame = contentScrollView.frame;
    //frame.origin.x = 320.0 * numPages;
    //frame.origin.y = 0;
   
    if(numPages == 1){
        [[appData getPageAt:pageControl.currentPage-1] updateQuality:kCGInterpolationLow];
        [self addPageHelper:image];
    }
    else{
        [UIView animateWithDuration:0.5
                         animations:^{
                             [contentScrollView setContentOffset:CGPointMake([appData screenWidth]*(numPages-1), 0)];
                         }completion:^(BOOL finished) {
                             [[appData getPageAt:pageControl.currentPage-1] updateQuality:kCGInterpolationLow];
                             [self addPageHelper:image];
                         }];
    }
    //    [contentScrollView scrollRectToVisible:frame animated:YES];

    [self dismissListView];
    //  [UIView animateWithDuration:0.5
    //                 animations:^ { [contentScrollView setContentOffset:CGPointMake(320*numPages, 0)]; }
    //               completion:nil];
    /*
     [UIView transitionWithView:contentScrollView
     duration:1.0
     options:UIViewAnimationOptionTransitionCurlDown  // 애니메이션 효과 입니다.
     animations:^ { [contentScrollView addSubview:page]; }
     completion:nil];
     */

    /*
    if(numPages == 1){
        [self addPageHelper:image];
    }
    else{
        [self performSelector:@selector(addPageHelper:) withObject:(UIImage*)image afterDelay:0.4];
    }
    */
    bToolbarShowing = false;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [self setToolbarHidden:YES];
//    [self.navigationController setToolbarHidden:YES animated:YES];
   
}

- (void) addPageHelper:(UIImage*)image{

    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapped:)];
    [tapRecognizer setNumberOfTapsRequired:2];
    [tapRecognizer setDelegate:self];
    
    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(shortPressed:)];
    [singleTapRecognizer setNumberOfTapsRequired:1];
    [singleTapRecognizer setDelegate:self];
    
    UILongPressGestureRecognizer *longPressRecognizer =[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [longPressRecognizer setMinimumPressDuration:0.1];
    [longPressRecognizer setDelegate:self];

    SinglePage *page = [[SinglePage alloc] initWithFrame:CGRectMake([appData screenWidth]*(numPages-1), 0, [appData screenWidth], [appData screenHeight])];
//    [page setUID:[appData getNewUID]];
   // UIImage *savedImage = [UIImage imageWithData:[AppData readFromFile:[NSString stringWithFormat:@"%d.jpeg", [page getUID]]]];
  //  savedImage?[page setBackgroundImage:savedImage]:[page setBackgroundImage:image];
    //image = nil;
    

    [page setUserInteractionEnabled:YES];
//      [[page panel] addGestureRecognizer:panRecognizer];
    [[page panel] addGestureRecognizer:tapRecognizer];
    [[page panel] addGestureRecognizer:singleTapRecognizer];
    [[page panel] addGestureRecognizer:longPressRecognizer];
    [page setTitle:@"Set Title" saveToDB:true];
//    int r = arc4random_uniform(1000) - 500;
//    [page setDate:[[NSDate date] dateByAddingTimeInterval:(r*60*60*24)] saveToDB:true];
    [page setDate:[NSDate date] saveToDB:true];
    
    // clear flags
    [page setBIsDateSet:false];
    [page setBIsTitleSet:false];
    [[page panel] setFrame:CGRectMake(arc4random_uniform(130)+10, arc4random_uniform(300)+10, [[page panel] frame].size.width, [[page panel] frame].size.height)];
    [appData pre_addEntry:page atPage:pageControl.currentPage];
    [page setBackgroundImage:image save:YES];
    
    [UIView transitionWithView:contentScrollView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown  // 애니메이션 효과 입니다.
                    animations:^ { [contentScrollView addSubview:page]; }
                    completion:^(BOOL finished){
                        // to-do: save image
//#warning image might not have been saved!!!
                        [appData addEntry:page atPage:pageControl.currentPage];
                        
                        // enable toolbar items
                        [toolbar_shareButton setEnabled:YES];
                        [toolbar_listButton setEnabled:YES];
                        [toolbar_deleteButton setEnabled:YES];
                        [toolbar_infoButton setEnabled:YES];

                        //numPages++;
                        
                        [page setBInView:true];
                        [page updateQuality:kCGInterpolationHigh];

                        //[listTableView reloadData];
                        
                      //  [[page multiDialController] spinToRandomString:YES];
                    }];

    if([toolbarItems objectAtIndex:0] != toolbar_shareButton)
        [toolbarItems replaceObjectAtIndex:0 withObject:toolbar_shareButton];
}


- (void)deleteEntryAtPage:(NSInteger)deletePage currentPage:(NSInteger)currentPage hideAtEnd:(bool)hideOrNot{
    [contentScrollView setUserInteractionEnabled:NO];

    if([appData getNumEntries] == 0){
        ;
    }
    else{
        NSLog(@"DELETEPAGE %d    CURRENTPAGE %d", deletePage, currentPage);
        SinglePage *target = [appData getPageAt:deletePage];
        [appData removeEntryAtPage:deletePage];
        
        if(numPages == 1){
            // inactivate delete
            [self performSelectorOnMainThread:@selector(disable_toolbarIcons) withObject:nil waitUntilDone:NO];
            
            //[UIView animateWithDuration:0.5
            //               animations:^{[contentScrollView viewWithTag:uidToDelete] .alpha = 0.0;}
            //           completion:^(BOOL finished){ [[contentScrollView viewWithTag:uidToDelete]  removeFromSuperview]; }];
            [UIView transitionWithView:contentScrollView
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCurlUp
                            animations:^ { [target removeFromSuperview]; } //[[contentScrollView viewWithTag:uidToDelete]  removeFromSuperview]; }
                            completion:^(BOOL finished){
                                [contentScrollView setUserInteractionEnabled:YES];
                                numPages = 0;
                                pageControl.numberOfPages = 0;
                                pageControl.currentPage = 0;
                                [self dismissListView];
                                [self postDeleteFunction_hide:hideOrNot];
                            }];
            return;
        }
         // when removal page is the last page
        else if(deletePage == (numPages-1)){
            if(currentPage == deletePage){
                [UIView transitionWithView:contentScrollView
                                  duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlUp  // 애니메이션 효과 입니다.
                                animations:^ { [target removeFromSuperview]; }
                                completion:^(BOOL finished){
                                    numPages--;
                                    pageControl.numberOfPages--;
                                    
                                    
                                    [UIView animateWithDuration:0.5
                                                     animations:^{
                                                         [contentScrollView setContentOffset:CGPointMake([appData screenWidth]*(currentPage-1), 0)];
                                                     }completion:^(BOOL finished) {
                                                         SinglePage *p = [appData getPageAt:currentPage-1];
                                                         [p setBInView:true];
                                                         [self performSelector:@selector(queueForHighQuality:) withObject:p afterDelay:0.2];
                                                         [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*numPages, [appData screenHeight])];
                                                         [self postDeleteFunction_hide:hideOrNot];
                                                     
                                                     }];

                                    
                                    
                                    /*
                                    [contentScrollView scrollRectToVisible:CGRectMake(320 * (currentPage-1), 0, SCREENWIDTH, SCREENHEIGHT) animated:YES];
                                    
                                    SinglePage *p = [appData getPageAt:currentPage-1];
                                    [p setBInView:true];
                                    [self performSelector:@selector(queueForHighQuality:) withObject:p afterDelay:0.3];
                                    
                                    [self postDeleteFunction_hide:hideOrNot];
                                     */
                                }];
            }
            else{
                [target removeFromSuperview];
                numPages--;
                pageControl.numberOfPages--;
                [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*numPages, [appData screenHeight])];
                [self postDeleteFunction_hide:hideOrNot];
            }
        }
            
        // when removal page is not the last page
        else{
            if(currentPage == deletePage){
                SinglePage *pageToMove = [appData getPageAt:deletePage];
                [UIView transitionWithView:contentScrollView
                                  duration:0.5
                                   options:UIViewAnimationOptionTransitionCurlUp  // 애니메이션 효과 입니다.
                                animations:^ { [target removeFromSuperview]; }
                                completion:^(BOOL finished){
                                    [UIView animateWithDuration:0.5
                                                     animations:^{
                                                        [pageToMove setFrame:CGRectOffset(pageToMove.frame, -[appData screenWidth], 0)];
                                                     }
                                                     completion:^(BOOL finished){
                                                         for(int i=deletePage+1; i<numPages - 1; i++){
                                                             [[appData getPageAt:i] setFrame:CGRectMake([appData screenWidth]*i, 0, [appData screenWidth], [appData screenHeight])];
                                                         }

                                                         numPages--;
                                                         pageControl.numberOfPages--;
                                                         //pageControl.currentPage--;
                                                         
                                                         [pageToMove setBInView:true];
                                                         [self performSelector:@selector(queueForHighQuality:) withObject:pageToMove afterDelay:0.2];
                                                         
                                                         [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*numPages, [appData screenHeight])];
                                                         [self postDeleteFunction_hide:hideOrNot];

                                                     }];
                                                                  
                                }];
                
            }

            else if(currentPage < deletePage){
                [target removeFromSuperview];
                for(int i=deletePage; i<numPages-1; i++)
                    [[appData getPageAt:i] setFrame:CGRectMake([appData screenWidth]*i, 0, [appData screenWidth], [appData screenHeight])];

                numPages--;
                pageControl.numberOfPages--;
                [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*numPages, [appData screenHeight])];
                [self postDeleteFunction_hide:hideOrNot];
            }
            
            else{
                [target removeFromSuperview];
              
                /*SinglePage *pageToMove1 = [appData getPageAt:currentPage-1];
                SinglePage *pageToMove2 = [appData getPageAt:currentPage];
                
                if(pageToMove2){
                    [UIView animateWithDuration:0.5
                                     animations:^{
                                         [pageToMove1 setFrame:CGRectOffset(pageToMove1.frame, -SCREENWIDTH, 0)];
                                         [pageToMove2 setFrame:CGRectOffset(pageToMove2.frame, -SCREENWIDTH, 0)];
                                     }
                                     completion:^(BOOL finished){
                                         for(int i=deletePage; i<numPages - 1; i++){
                                             if(i==currentPage || i==currentPage-1)
                                                 continue;
                                             SinglePage *p = [appData getPageAt:i];
                                             [p setFrame:CGRectOffset(p.frame, -SCREENWIDTH, 0)];
                                         }
                                         numPages--;
                                         pageControl.numberOfPages--;
                                         [contentScrollView setContentSize:CGSizeMake(SCREENWIDTH*numPages, SCREENHEIGHT)];

                                         [pageToMove2 setBInView:true];
                                         [self performSelector:@selector(queueForHighQuality:) withObject:pageToMove2 afterDelay:0.2];
                                         
                                         [self postDeleteFunction_hide:hideOrNot];
                                     }];
                }
                 */
                //else{
                NSLog(@"before delete: %f", [contentScrollView contentOffset].x);
                for(int i=deletePage; i<numPages-1; i++){
                          [[appData getPageAt:i] setFrame:CGRectMake([appData screenWidth]*i, 0, [appData screenWidth], [appData screenHeight])];
                }
                [contentScrollView setContentOffset:CGPointMake([appData screenWidth]*(currentPage-1), 0)];
                    numPages--;
                    pageControl.numberOfPages--;
                    //pageControl.currentPage--;
                    [contentScrollView setContentSize:CGSizeMake([appData screenWidth]*numPages, [appData screenHeight])];
                NSLog(@"after delete: %f", [contentScrollView contentOffset].x);
                [self postDeleteFunction_hide:hideOrNot];
                //}
            }
        }
    }
}



- (void) postDeleteFunction_hide:(bool)hideOrNot{
    [toolbar_addButton setEnabled:YES];
    
    [contentScrollView setUserInteractionEnabled:YES];
    
    bToolbarShowing = hideOrNot?true:false;
    
    if([[appData entries] count] == 0){
        [self.navigationController setToolbarHidden:YES animated:NO];
        [toolbarItems replaceObjectAtIndex:0 withObject:toolbar_helpButton];
        [self.navigationController setToolbarHidden:NO animated:NO];
        
    }
    
    if(hideOrNot)
        [self setToolbarHidden:YES];
//        [self.navigationController setToolbarHidden:YES animated:YES];
    

}


#pragma mark - Info and settings
//=========================================
//  Info and Settings
//=========================================

-(void)openInfoPage:(id)sender{
    InfoPageViewController *infopageVC = [[InfoPageViewController alloc] init];
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:infopageVC];
        // set current page info here
    controller.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    // [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [infopageVC setDelegate:self];
    [self presentViewController:controller animated:YES completion:nil];
    bToolbarShowing = false;
    [self.navigationController setToolbarHidden:YES animated:YES];
    NSLog(@"ENDED");
}

-(void)openSettingsPage:(id)sender{
    SettingsViewController *settingsPageVC = [[SettingsViewController alloc] init];
    settingsPageVC.delegate = self;
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:settingsPageVC];
    // set current page info here
    controller.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    // [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:controller animated:YES completion:nil];
    bToolbarShowing = false;
    [self.navigationController setToolbarHidden:YES animated:YES];
}




#pragma mark - Delegate methods

//=========================================
//   Scroll Delegate methods
//=========================================
- (void) scrollViewDidEndDecelerating{
    //[self queueForShowing:[NSNumber numberWithInt:pageControl.currentPage]];
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [p setBInView:true];
    [self performSelector:@selector(queueForHighQuality:) withObject:p afterDelay:0.2];
    
//    [self performSelectorInBackground:@selector(queueForShowing:) withObject:[NSNumber numberWithInt:pageControl.currentPage] ];
   
}

- (void) scrollViewWillBeginDragging{

    if(numPages == 1)
        return;
    
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [self performSelectorInBackground:@selector(queueForLowQuality:) withObject:p];
}

- (void) queueForHighQuality:(SinglePage*)page {
    [page updateQuality:kCGInterpolationHigh];
}

- (void) queueForLowQuality:(SinglePage*)page {
    [page updateQuality:kCGInterpolationLow];
    [page setBInView:false];
}

//=========================================
//   InfoPageViewController Delegate methods
//=========================================

- (void) updateImage:(UIImage*)newImage{
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [p setBackgroundImage:newImage save:YES];
    [listTableView reloadData];

}

- (void) updateTitle:(NSString*)newTitle{
    [[appData getPageAt:pageControl.currentPage] setTitle:newTitle saveToDB:true];
    [listTableView reloadData];
}

- (void) updateDate:(NSDate*)newDate{
    [[appData getPageAt:pageControl.currentPage] setDate:newDate saveToDB:true];
    [listTableView reloadData];
    
}

- (bool)isDateSet{
    return [[appData getPageAt:pageControl.currentPage] isDateSet];
}

- (NSDate*) getDate{
    return [appData stringToDate:[[appData getPageAt:pageControl.currentPage] getDateString]];

}

- (NSString*) getDateStr{
        return [[appData getPageAt:pageControl.currentPage] getDateString];
}

- (NSString *)getTitle{
    return [[appData getPageAt:pageControl.currentPage] getTitle];
}

- (bool)isTitleSet{
    return [[appData getPageAt:pageControl.currentPage] isTitleSet];
};

- (bool)isAlarmSet{
    return [[appData getPageAt:pageControl.currentPage] bAlarmEnabled];
}

- (void)updateAlarmSet:(bool)value{
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [p setBAlarmEnabled:value];
    // update db
    [appData db_updateAlarm:p];
    // schedule local notification
    [appData updateNotification:p];
}

- (NSInteger) getAlarmDays{
    return [[appData getPageAt:pageControl.currentPage] getAlarmDays];
}

- (void) updateAlarmDays:(NSInteger)days{
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [p setAlarmDays:days];
    [appData db_updateAlarm:p];
    [appData updateNotification:p];
}


- (NSDate*) getAlarmTime{
    return [[appData getPageAt:pageControl.currentPage] getAlarmTime];
}

- (void) updateAlarmTime:(NSDate*)time{
    SinglePage *p = [appData getPageAt:pageControl.currentPage];
    [p setAlarmTime:time];
    [appData db_updateAlarm:p];
    [appData updateNotification:p];
}

- (void) doneCompleted{
   // [[[appData getPageAt:pageControl.currentPage] multiDialController] spinToRandomString:YES];
    //[self dismissListView];
    [listTableView reloadData];
}

- (void) reset{
    
    [self performSelectorOnMainThread:@selector(disable_toolbarIcons) withObject:nil waitUntilDone:NO];
    
    for(SinglePage *p in [appData entries]){
        [p removeFromSuperview];
    }
    [[appData entries] removeAllObjects];
    numPages = 0;
    [toolbar_addButton setEnabled:YES];
    pageControl.currentPage=-1;
    pageControl.numberOfPages=0;
    
    [toolbarItems replaceObjectAtIndex:0 withObject:toolbar_helpButton];
    [self.navigationController setToolbarHidden:NO animated:NO];
    bToolbarShowing = true;
    
    [appData resetAll];
    [listTableView reloadData];
    
}

- (void) reloadPanels{
    for(SinglePage *p in [appData entries]){
        [p drawPanel];
    }
}

#pragma mark - Listview
//=========================================
//   List : TableView related methods
//=========================================

UIView *listView;
UITableView *listTableView;

- (void) showList{
    bListviewShowing = true;
    
    [contentScrollView setUserInteractionEnabled:NO];
    if(listView == nil){
        listView = [self createListView];
        [self.view addSubview:listView];
    }
    [listTableView reloadData];
    
    [UIView transitionWithView:listView duration:0.2 options:UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        [listView setFrame:CGRectMake(10, 40, 300, [appData screenHeight]-90)];
                    }
                    completion:nil];
 
    bToolbarShowing = false;
    [self.navigationController setToolbarHidden:YES animated:YES];
    
}
- (void)dismissListView{
    bListviewShowing = false;
    [self editListView:false];
    [contentScrollView setUserInteractionEnabled:YES];
    [UIView transitionWithView:listView duration:0.2 options:UIViewAnimationOptionCurveEaseIn
                    animations:^{
                        [listView setFrame:CGRectMake(10, [appData screenHeight], 300, [appData screenHeight]-90)];
                    }
                    completion:nil];
}

- (UIView*) createListView{
    UIView *listViewContainer = [[UIView alloc] initWithFrame:CGRectMake(10, [appData screenHeight], 300, [appData screenHeight]-90)];
//    [listViewContainer setBackgroundColor:[UIColor whiteColor]];
    [listViewContainer setBackgroundColor:[UIColor clearColor]];
    [listViewContainer setTag:TOUCH_IGNORE];
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, [appData screenHeight]-90)];
    [bg setBackgroundColor:[UIColor blackColor]];
    [bg setAlpha:0.6];
    [bg setUserInteractionEnabled:NO];
    [listViewContainer addSubview:bg];

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"Button_close"] forState:UIControlStateNormal];
    [button setFrame:CGRectMake(5,5,25,25)];
    [button addTarget:self action:@selector(dismissListView) forControlEvents:UIControlEventTouchUpInside];
    [listViewContainer addSubview:button];
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [editButton setImage:[UIImage imageNamed:@"Button_Edit"] forState:UIControlStateNormal]; //139x53
    [editButton setFrame:CGRectMake(220,5,65,25)];
    [editButton addTarget:self action:@selector(editListViewToggle) forControlEvents:UIControlEventTouchUpInside];
    [listViewContainer addSubview:editButton];
   
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sortButton setImage:[UIImage imageNamed:@"Button_sort"] forState:UIControlStateNormal]; //139x53
    [sortButton setFrame:CGRectMake(110,5,65,25)];
    [sortButton addTarget:self action:@selector(sort) forControlEvents:UIControlEventTouchUpInside];
    [listViewContainer addSubview:sortButton];
   
    
    /*
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[editButton setImage:[UIImage imageNamed:@"document_64"] forState:UIControlStateNormal];
    [editButton setFrame:CGRectMake(200,5,100,30)];
    [editButton addTarget:self action:@selector(editListViewToggle) forControlEvents:UIControlEventTouchUpInside];
    [listViewContainer addSubview:editButton];
    */
    
    /*
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [sortButton setTitle:@"Sort" forState:UIControlStateNormal];
    [sortButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sortButton setFrame:CGRectMake(100,5,100,30)];
    [sortButton setBackgroundColor:[UIColor clearColor]];
    [sortButton addTarget:self action:@selector(sort) forControlEvents:UIControlEventTouchUpInside];
    [listViewContainer addSubview:sortButton];
    */
    
    listTableView = [[UITableView alloc] initWithFrame:CGRectMake(10,30,280,[appData screenHeight]-90-40)];
    [listTableView setBackgroundColor:[UIColor clearColor]];
    [listViewContainer addSubview:listTableView];
    [listTableView setDelegate:self];
    [listTableView setDataSource:self];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
    [listTableView setTableFooterView:v];

//    [listTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [listTableView setEditing:NO animated:YES];
    return listViewContainer;
    
}



bool bEditListView = false;
- (void)editListView:(bool)editEnabled{
    bEditListView = !editEnabled;
    [self editListViewToggle];
}

- (void)editListViewToggle{
    if(bEditListView){
        [listTableView setEditing:NO animated:YES];
        bEditListView = false;
    }
    else{
        [listTableView setEditing:YES animated:YES];
        bEditListView = true;
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"num entries %d", [appData getNumEntries]);
    return [appData getNumEntries];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    TopPageListviewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    if(cell == nil){
        cell = [[TopPageListviewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    SinglePage *page = [appData getPageAt:indexPath.row];
    UIImage *newimg = [imageCache objectForKey:[NSString stringWithFormat:@"%d", [page getUID]]];

    if(!newimg){
        
        NSLog(@"no image");
        if([page getBgImage] == NULL)
            NSLog(@"bgImage null!!!");
         newimg = [[page getBgImage] resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                              bounds:CGSizeMake(88, 88)
                                                interpolationQuality:kCGInterpolationMedium];
        NSLog(@"Fetch new image");
        if(!newimg){
            NSLog(@"NO IMAGE RETURNED");
        }

        [imageCache setObject:newimg forKey:[NSString stringWithFormat:@"%d", [page getUID]]];
        NSLog(@"cached");
    }
    //UIImage *newimg = [SinglePage imageWithImage:[page getBgImage] scaledToSize:CGSizeMake(44, 44)];
    [cell.image_  setImage:newimg];

    NSString *daysLeft;
    NSString *eventUntilOrSince;
    
    if([page getEventDaysLeft] == 0){
        daysLeft = @"Today";
        eventUntilOrSince = @"";
    }
    else{
        if([page getDdayPast] && [appData getCountFromOne]){
            daysLeft = [NSString stringWithFormat:@"%d", [page getEventDaysLeft]+1];
            switch (([page getEventDaysLeft]+1) % 10) {
                case 1:
                    eventUntilOrSince = @"st day since";
                    break;
                case 2:
                    eventUntilOrSince = @"nd day since";
                    break;
                case 3:
                    eventUntilOrSince = @"rd day since";
                    break;
                default:
                    eventUntilOrSince = @"th day since";
                    break;
            }
        }
        else{
            daysLeft = [NSString stringWithFormat:@"%d", [page getEventDaysLeft]];
            eventUntilOrSince = [page getDdayPast]?@"days since":@"days until";
        }
    }
    
    NSString* outtxt = [NSString stringWithFormat:@"%@ %@", daysLeft, eventUntilOrSince];
    NSMutableAttributedString *attrStr = [NSMutableAttributedString attributedStringWithString:outtxt];
  //  [attrStr setFont:[UIFont systemFontOfSize:15]];
    [attrStr setFont:[UIFont fontWithName:@"Helvetica" size:15]];
    [attrStr setTextBold:YES range:[outtxt rangeOfString:daysLeft]];
    [attrStr setTextColor:[UIColor whiteColor]];
    
    [[cell topLeftLabel] setAttributedText:attrStr];
    [[cell topRightLabel] setText:[page getDateStringShort]];
    [[cell titleLabel] setText:[page getTitle]];

    cell.textLabel.text = @"whatever";
    cell.textLabel.hidden = YES;
    return cell;
}



 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
     
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Delete the row from the data source
         [self deleteEntryAtPage:indexPath.row currentPage:pageControl.currentPage hideAtEnd:false];
         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         
     }
 //else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 //}
 }
 


 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
     [UIView animateWithDuration:0.5
                      animations:^{
                          [[appData getPageAt:fromIndexPath.row] setFrame:CGRectMake([appData screenWidth]*toIndexPath.row, 0, [appData screenWidth], [appData screenHeight])];
                         
                          if(fromIndexPath.row > toIndexPath.row){
                              for(int i=toIndexPath.row; i<fromIndexPath.row; i++)
                                  [[appData getPageAt:i] setFrame:CGRectMake([appData screenWidth]*(i+1), 0, [appData screenWidth], [appData screenHeight])];
                          }
                          else if(fromIndexPath.row < toIndexPath.row){
                              for(int i=fromIndexPath.row+1; i<=toIndexPath.row; i++)
                                  [[appData getPageAt:i] setFrame:CGRectMake([appData screenWidth]*(i-1), 0, [appData screenWidth], [appData screenHeight])];
                          }
                      }
                      completion:^(BOOL finished) {
                          [appData moveEntryAtPage:fromIndexPath.row toPage:toIndexPath.row];
                          
                      }];
     
     
 }
 
- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];

}
/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissListView];
    
    if(pageControl.currentPage == indexPath.row)
        return;
    
    int previousPage = pageControl.currentPage;
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [contentScrollView setContentOffset:CGPointMake([appData screenWidth]*indexPath.row, 0)];
                     }completion:^(BOOL finished) {
                         //pageControl.currentPage = indexPath.row;
                         [self performSelectorInBackground:@selector(queueForLowQuality:) withObject:[appData getPageAt:previousPage]];
                         SinglePage *p = [appData getPageAt:pageControl.currentPage];
                         [p setBInView:true];
                         [self performSelector:@selector(queueForHighQuality:) withObject:p afterDelay:0.2];
                     }];
    
    /*
    CGRect frame = contentScrollView.frame;
    frame.origin.x = 320.0 * indexPath.row;
    frame.origin.y = 0;
    
    [listTableView deselectRowAtIndexPath:indexPath animated:YES];
    [contentScrollView scrollRectToVisible:frame animated:YES];
     */
}

- (void)sort{
    [self editListView:false];
    [appData sort];
    for(SinglePage *p in [appData entries])
        [p setAlpha:0.7];

    [UIView animateWithDuration:0.8
                     animations:^{
                         for(int i=0; i<numPages; i++){
                             int v = [appData getOrderAtPage:i];
                             [[appData getPageAt:v] setFrame:CGRectMake([appData screenWidth]*i, 0, [appData screenWidth], [appData screenHeight])];
                             [listTableView setEditing:NO animated:NO];

                         }
                     }
                     completion:^(BOOL finished) {
                         for(SinglePage *p in [appData entries])
                             [p setAlpha:1.0];
                         [appData sortCompleted];
                         [listTableView reloadData];
                     }];
}





@end
